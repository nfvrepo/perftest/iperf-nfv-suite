#!/usr/bin/env bash
########################################################################################
# File           : iperf-nfv-ts-client.sh
# Version        : 0.5.0
# Author(s)      : Erol Ozcan <https://gitlab.com/erol.ozcan> (Argela)
# Create date    : 2018-06-02
# Description    :
#
#########################################################################################
TMP_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TMP_SCRIPT_FNAME=$(basename $0)

OPER="${1}"
PRM02="${2}"
PRM03="${3}"
PRM04="${4}"
PRM05="${5}"
PRM06="${6}"

[[ -e bash-util-common-lib.sh ]] && source bash-util-common-lib.sh || source ${TMP_SCRIPT_DIR}/bash-util-common-lib.sh
[[ -e bash-util-common-lib.sh ]] && source iperf-nfv-ts-lib.sh || source ${TMP_SCRIPT_DIR}/iperf-nfv-ts-lib.sh

######################
# write_pdata_header
######################
write_pdata_header() {
    local l_pdata_file="${1}"

    echo -e "#testname\tsent_mbps\treceived_mbps\tcpu_util_client\tcpu_util_server" \
"\tsent_packets\tsent_bytes\tsent_seconds" \
"\ttcp_received_packets\ttcp_recieved_bytes\ttcp_received_seconds\ttcp_sent_retransmits" \
"\tudp_lost_packets\tudp_lost_percent\tudp_jitter_ms" \
     > "${l_pdata_file}"
}

######################
# save_pdata_from_json
######################
save_pdata_from_json() {
    local l_json_file="$1"
    local l_pdata_file="$2"

    local l_file_noext=$(basename "$l_json_file" .json)
    local l_test_name="${l_file_noext:0:${#l_file_noext}-15}"
    echo "save_pdata_from_json $l_json_file $l_pdata_file"
    local l_udp_enbaled=0
    if [[ $l_file_noext = *"udp"* ]]; then
        l_udp_enbaled=1
    fi

    local l_sent_mbps="1"
    local l_received_mbps="1"
    local l_sent_packets="0"
    local l_sent_bytes="0"
    local l_sent_seconds="0"
    local l_tcp_received_bytes="0"
    local l_tcp_received_packets="0"
    local l_tcp_received_seconds="0"
    local l_tcp_sent_retransmits="0"
    local l_udp_lost_packets="0"
    local l_udp_lost_percent="0"
    local l_udp_jitter_ms="0"

    local l_cpu_util_client=$(jq '.end.cpu_utilization_percent.host_total' < "$l_json_file" | xargs printf "%0.f\n")
    local l_cpu_util_server=$(jq '.end.cpu_utilization_percent.remote_total' < "$l_json_file" | xargs printf "%0.f\n")

    if [ ${l_udp_enbaled} -eq 1 ]; then
        local l_sent_bps=$(jq '.end.sum.bits_per_second / 1.0' < "$l_json_file" | xargs printf "%0.f\n")
        if [[ "$l_sent_bps" -gt "$ONE_MBIT" ]]; then
           l_sent_mbps=$(jq '.end.sum.bits_per_second / 1000.0 / 1000.0' < "$l_json_file" | xargs printf "%0.f\n")
        fi
        l_sent_packets=$(jq '.end.sum.packets' < "$l_json_file" | xargs printf "%0.f\n")
        l_sent_bytes=$(jq '.end.sum.bytes' < "$l_json_file" | xargs printf "%0.f\n")
        l_sent_seconds=$(jq '.end.sum.seconds' < "$l_json_file" | xargs printf "%0.f\n")
        l_udp_lost_packets=$(jq '.end.sum.lost_packets' < "$l_json_file" | xargs printf "%0.f\n")
        l_udp_lost_percent=$(jq '.end.sum.lost_percent' < "$l_json_file" | xargs printf "%0.2f\n")
        l_udp_jitter_ms=$(jq '.end.sum.jitter_ms' < "$l_json_file" | xargs printf "%0.5f\n")
   else
        local sent_bps=$(jq '.end.sum_sent.bits_per_second / 1.0' < "$l_json_file" | xargs printf "%0.f\n")
        if [[ "$sent_bps" -gt "$ONE_MBIT" ]]; then
           l_sent_mbps=$(jq '.end.sum_sent.bits_per_second / 1000.0 / 1000.0' < "$l_json_file" | xargs printf "%0.f\n")
        fi
        local l_received_bps=$(jq '.end.sum_received.bits_per_second / 1.0' < "$l_json_file" | xargs printf "%0.f\n")
        if [[ "$l_received_bps" -gt "$ONE_MBIT" ]]; then
           l_received_mbps=$(jq '.end.sum_received.bits_per_second / 1000.0 / 1000.0' < "$l_json_file" | xargs printf "%0.f\n")
        fi

        #l_sent_packets=$(jq '.end.sum_sent.packets' < "$l_json_file" | xargs printf "%0.f\n")
        l_sent_bytes=$(jq '.end.sum_sent.bytes' < "$l_json_file" | xargs printf "%0.f\n")
        l_sent_seconds=$(jq '.end.sum_sent.seconds' < "$l_json_file" | xargs printf "%0.f\n")
        l_tcp_sent_retransmits=$(jq '.end.sum_sent.retransmits' < "$l_json_file" | xargs printf "%0.f\n")

        #l_tcp_received_packets=$(jq '.end.sum_received.packets' < "$l_json_file" | xargs printf "%0.f\n")
        l_tcp_received_bytes=$(jq '.end.sum_received.bytes' < "$l_json_file" | xargs printf "%0.f\n")
        l_tcp_received_seconds=$(jq '.end.sum_received.seconds' < "$l_json_file" | xargs printf "%0.f\n")
   fi

    echo -e "${l_test_name}\t${l_sent_mbps}\t${l_received_mbps}\t${l_cpu_util_client}\t${l_cpu_util_server}" \
"\t${l_sent_packets}\t${l_sent_bytes}\t${l_sent_seconds}" \
"\t${l_tcp_received_packets}\t${l_tcp_received_bytes}\t${l_tcp_received_seconds}\t${l_tcp_sent_retransmits}" \
"\t${l_udp_lost_packets}\t${l_udp_lost_percent}\t${l_udp_jitter_ms}" \
      >> ${l_pdata_file}
}

######################
# save_pdata_files
######################
save_pdata_files() {
   local l_save_dir="${1}"
   local l_json_files_pattern=${@:2}

    if [[ -z "${l_save_dir// }" ]]; then
       echo "Usage: $0 save-pdata-files [save-dir] [json-files-pattern]"
       exit 1
    fi
    if [[ -z "${l_json_files_pattern// }" ]]; then
       echo "Usage: $0 save-pdata-files [save-dir] [json-files-pattern]"
       exit 1
    fi

    if [ ! -e "${l_save_dir}" ]; then
       echo "Warning: creating missing save directory ${l_save_dir}."
       mkdir -p "${l_save_dir}"
    fi


    local l_header_saved_files=()
    local l_json_file=""
    for l_json_file in ${l_json_files_pattern[@]}; do
        local l_file_noext=$(basename "$l_json_file" .json)
        local l_test_date="${l_file_noext: -14}"
        local l_test_name="${l_file_noext:0:${#l_file_noext}-15}"

        local l_ts_name=""
        local l_udp_enbaled=0
        if [[ $l_file_noext = *"udp"* ]]; then
            l_udp_enbaled=1
            l_ts_name="${l_test_name%-*}"
        else
            local l_ts_name="${l_test_name%-*}"
            l_ts_name="${l_test_name%-*}"
            l_ts_name="${l_ts_name%-*}"
        fi
        local l_test_name="${l_file_noext:0:${#l_file_noext}-15}"

        #local l_ts_name="${l_test_name%-*}"
        local l_pdata_file_noext="${l_save_dir}/${l_ts_name}-${l_test_date}"
        local l_pdata_file="${l_pdata_file_noext}.pdata"
        local l_info_file="${l_pdata_file_noext}.info"

        elem_exists_in_array "${l_pdata_file}" ${l_header_saved_files[@]}
        local l_rc=$?
        echo "DEBUG: $l_pdata_file exists  l_header_saved_files rc: $l_rc"
        if [[ "${l_rc}" -ne "0" ]]; then
            write_pdata_header "${l_pdata_file}"
            l_header_saved_files+=("${l_pdata_file}")
            #echo "DEBUG: l_header_saved_files=${l_header_saved_files[@]}"
        fi

        echo "INFO: File: ${l_json_file}, l_pdata_file=${l_pdata_file}, l_test_name=${l_test_name}, l_test_date=${l_test_date}, l_ts_name=${l_ts_name}"

        save_pdata_from_json "${l_json_file}" "${l_pdata_file}"
    done

}

######################
# run_iperf_client
######################
run_iperf_client() {
    local l_parallel="$1"
    local l_mss="$2" #  ethheader + ipheader + tcpheader
    local l_ts_start_date="${3}"

    local l_start_date=`date +${DATE_FMT}`
    local l_test_name="${TESTSUITE_NAME}-p${l_parallel}"
    if [[ "${UDP_ENABLED}" -ne "1" ]] && [[ ! -z "${l_mss// }" ]]; then
      l_test_name="${l_test_name}-mss${l_mss}"
    fi

    mkdir -p "${RESULT_DIR}"
    local l_json_out_file="${RESULT_DIR}/${l_test_name}-${l_ts_start_date}.json"
    local IPERF_ARGS=()

    if [ ${UDP_ENABLED} -eq 1 ]; then
      IPERF_ARGS+=("-u")
    elif [[ ! -z "${l_mss// }" ]]; then
        IPERF_ARGS+=("-M" "${l_mss}")
    fi

    IPERF_ARGS+=("-4" "--get-server-output")
    #IPERF_ARGS+=("-Z" "-4" "--get-server-output")
    #IPERF_ARGS+=("-V" "-l" "8944" "-w" "2097152" "-4" "--get-server-output")
    [[ ! -z "${CLIENT_AFFINITY// }" ]] && IPERF_ARGS+=("-A" "${CLIENT_AFFINITY}")

    if [[ ! -z "${BANDWITH// }" ]]; then
        local l_bw_by_parallel_stream=$(iperf_bandwith_calc ${BANDWITH} ${l_parallel})
        IPERF_ARGS+=("-b" "${l_bw_by_parallel_stream}")
    fi
    [[ ! -z "${l_parallel// }" ]] && IPERF_ARGS+=("-P" "${l_parallel}")
    IPERF_ARGS+=("-t" "${TIME}")
    [[ ! -z "${OMIT_TIME// }" ]] && IPERF_ARGS+=("-O" "${OMIT_TIME}")
    IPERF_ARGS+=("-c" "${SERVER_IP}")
    IPERF_ARGS+=("-T" "${l_test_name}")
    IPERF_ARGS+=("-J")
    
    echo "DEBUG: ${IPERF_ARGS[@]/#/}"

    local l_cmd=""
    if [[ ! -z "${CLIENT_TASKSET// }" ]]; then
        l_cmd="taskset -c ${CLIENT_TASKSET} "
        #l_cmd="taskset ${CLIENT_TASKSET} iperf3 ${IPERF_ARGS[@]/#/} > ${l_json_out_file}"
        #else l_cmd="iperf3 ${IPERF_ARGS[@]/#/} > ${l_json_out_file}"
    fi
    l_cmd="${l_cmd} iperf3 ${IPERF_ARGS[@]/#/} > ${l_json_out_file}"

    local l_info_file="${RESULT_DIR}/${l_test_name}-${l_ts_start_date}.info"
    cat << ARGEOF > "${l_info_file}"
MAIN_TITLE="${MAIN_TITLE}"
TESTSUITE_NAME="${TESTSUITE_NAME}"
TITLE="${TITLE}"
TEST_NAME="${l_test_name}"
CLIENT_HOSTNAME="${CLIENT_HOSTNAME}"
CLIENT_IP="${CLIENT_IP}"
CLIENT_TASKSET="${CLIENT_TASKSET}"
CLIENT_AFFINITY="${CLIENT_AFFINITY}"
SERVER_IP="${SERVER_IP}"
SERVER_MGMT_IP="${SERVER_MGMT_IP}"
SERVER_TASKSET="${SERVER_TASKSET}"
SERVER_AFFINITY="${SERVER_AFFINITY}"
BANDWITH="${BANDWITH}"
UDP_ENABLED="${UDP_ENABLED}"
TIME=${TIME}
OMIT_TIME=${OMIT_TIME}
PARALLEL=${l_parallel}
MSS=${l_mss}
START_DATE="${l_start_date}"
CLIENT_CMD="${l_cmd}"
ARGEOF

    echo -e "Test started: $l_title\n\t$TITLE_DETAIL"
    run_cmd "${l_cmd}"

    if jq -e '.error' < "${l_json_out_file}"; then
        echo $(readlink -f $l_json_out_file) has an error, skipping >&2
        cat $l_json_out_file >&2
    else
        local l_pdata_file="${RESULT_DIR}/${TESTSUITE_NAME}-${l_ts_start_date}.pdata"
        save_pdata_from_json "${l_json_out_file}" "${l_pdata_file}"
    fi
     
    local l_end_date=`date +${DATE_FMT}`
    echo "END_DATE=${l_end_date}" >> "${l_info_file}"
    echo -e "Test finished: ${l_test_name}\n\t$l_end_date"
}

######################
# run_testsuite
######################
run_testsuite() {
    local l_profile="${1}"
    local l_conf_filename="${2}"

    if [[ -z "${l_profile// }" ]]; then
       echo "Usage: $0 run-testsuite [profile] [conf-filename]"
       exit 1
    fi
    if [[ ! -d "${INTS_ETC_DIR}/${l_profile}" ]]; then
       echo "ERROR: ${l_profile} profile does not exists!"
       exit 2
    fi

    read_ts_config "${l_profile}" "${l_conf_filename}"

    local l_ts_start_date=$(date +${DATE_FMT})

    echo "Test suite $TESTSUITE_NAME started at ${l_ts_start_date}"
    local l_mss_list_len=${#MSS_LIST[@]}
    echo "l_mss_list_len=${l_mss_list_len}"

    local l_pdata_file="${RESULT_DIR}/${TESTSUITE_NAME}-${l_ts_start_date}.pdata"
    write_pdata_header "${l_pdata_file}"

    local l_ts_info_file="${RESULT_DIR}/${TESTSUITE_NAME}-${l_ts_start_date}.info"
    echo "DEBUG: l_ts_info_file=$l_ts_info_file"
    cat << ARGEOF > "${l_ts_info_file}"
MAIN_TITLE="${MAIN_TITLE}"
TESTSUITE_NAME="${TESTSUITE_NAME}"
TITLE="${TITLE}"
CLIENT_HOSTNAME="${CLIENT_HOSTNAME}"
CLIENT_TASKSET="${CLIENT_TASKSET}"
CLIENT_AFFINITY="${CLIENT_AFFINITY}"
SERVER_IP="${SERVER_IP}"
SERVER_MGMT_IP="${SERVER_MGMT_IP}"
SERVER_TASKSET="${SERVER_TASKSET}"
SERVER_AFFINITY="${SERVER_AFFINITY}"
BANDWITH="${BANDWITH}"
UDP_ENABLED=${UDP_ENABLED}
TIME=${TIME}
OMIT_TIME=${OMIT_TIME}
START_DATE="${l_ts_start_date}"
ARGEOF

    local l_sys_info_filename="${TESTSUITE_NAME}-${l_ts_start_date}.sysinfo"
    write_sys_info "${l_sys_info_filename}" "${CLIENT_NIC}"

    local l_mss="0"
    local l_parallel="0"
    if [ "${l_mss_list_len}" -gt "0" ] && [ "${UDP_ENABLED}" -ne "1" ]; then
        for l_mss in ${MSS_LIST[@]}; do
            for l_parallel in "${PARALLEL_LIST[@]}"; do
                echo "INFO: MSS: ${l_mss}, PARALLEL: ${l_parallel}"
                run_iperf_client "${l_parallel}" "${l_mss}" "${l_ts_start_date}"
            done
        done
    else
        for l_parallel in ${PARALLEL_LIST[@]}; do
            #echo "m: $MSS_VAL, p:$PARALEL_VAL"
            run_iperf_client "${l_parallel}" "" "${l_ts_start_date}"
        done
    fi

    local l_end_date=`date +${DATE_FMT}`
    echo "END_DATE=\"${l_end_date}\"" >> "${l_ts_info_file}"

    echo "Finished tests @ ${l_end_date}"
}

# *****************************
# usage
# *****************************
usage() {
    cat << ARGEOF
Usage: $0 <run-testsuite|save-pdata-files> [other_params]
ARGEOF
}


ints_mkdirs
case "$OPER" in
   run-testsuite)
       # PRM02: profile, PRM03: config file
        run_testsuite ${PRM02} ${PRM03}
        ;;
   save-pdata-files)
        # PRM02: save dir , @:3 json files patterns
        save_pdata_files ${PRM02} ${@:3}
        ;;
   *)
        usage
        ;;
esac
