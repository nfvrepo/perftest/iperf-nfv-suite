#!/usr/bin/env bash
########################################################################################
# File           : iperf-nfv-ts-openstack.sh
# Version        : 0.3.0
# Author(s)      : Erol Ozcan <https://gitlab.com/erol.ozcan> (Argela)
# Create date    : 2018-05-02
# Description    :
# Bash common utility script.
# On kosullar:

# - <affinity-grp> , <anti-affinity-grp>, server policy grupları yaratılmış olmalıdır.
#openstack server group create --policy anti-affinity anti-affinity-grp
#openstack server group create --policy affinity affinity-grp
#openstack server group list

# - Test yapılacak ağ ortamına uygun flavor tanımlanmış olmalıdır (örnek DPDK için hugepage li)
# - test yapılacak ağda affinity-grp da
#########################################################################################

#MY_CONF_FILE="${1}"
OPER="${1}"
PRM02="${2}"
PRM03="${3}"
PRM04="${4}"
PRM05="${5}"
PRM06="${6}"

source bash-util-common-lib.sh
source iperf-nfv-ts-lib.sh

ints_mkdirs

# *****************************
# usage
# *****************************
usage() {
    cat << ARGEOF
Usage: $0 <ostack-iperf-ts-conf-file>
      <init-profile|create-affi-groups|create-net-ports|create-iperf-test-vms
       |create-iperf-affi-vms|create-iperf-anti-affi-vms
       |delete-iperf-test-vms|delete-iperf-affi-vms|delete-iperf-anti-affi-vms>
      [other_params]
ARGEOF
}

# *****************************
# init_profile
# *****************************
init_profile() {
    local l_profile="${1}"
    local l_openrc_file="${2}"

    check_profile ${l_profile}
    source "${l_openrc_file}"

    create_net_ports
    set_my_ostack_ids

    envsubst < ${INTS_ETC_DIR}/${l_profile}/ostack.conf.tpl > ${INTS_ETC_DIR}/${l_profile}/ostack.conf
    echo "Init profile ${l_profile_name} : done."
}

# *****************************
# source_ostack_conf
# *****************************
source_ostack_conf() {
    local l_profile_name="${1}"

    if [[ ! -e "${INTS_ETC_DIR}/${l_profile_name}/ostack.conf" ]]; then
        echo "WARN: ${l_profile_name} profile dir does not exists!"
        exit 2
    fi


}

# *****************************
# create_net_ports
# *****************************
create_net_ports() {
    local tmp_id=$(openstack port show ${AFF_VM1_PORT1_NAME} -f value -c id  2> /dev/null)
    if [[ -z "${tmp_id// }" ]]; then
        run_cmd "openstack port create --network ${PERF_NET_NAME} ${AFF_VM1_PORT1_NAME}"
    else
        echo "WARN: ${AFF_VM1_PORT1_NAME} exists."
    fi

    tmp_id=$(openstack port show ${AFF_VM2_PORT1_NAME} -f value -c id  2> /dev/null)
    if [[ -z "${tmp_id// }" ]]; then
        run_cmd "openstack port create --network ${PERF_NET_NAME} ${AFF_VM2_PORT1_NAME}"
    else
        echo "WARN: ${AFF_VM2_PORT1_NAME} exists."
    fi

    tmp_id=$(openstack port show ${ANTI_AFF_VM1_PORT1_NAME} -f value -c id  2> /dev/null)
    if [[ -z "${tmp_id// }" ]]; then
        run_cmd "openstack port create --network ${PERF_NET_NAME} ${ANTI_AFF_VM1_PORT1_NAME}"
    else
        echo "WARN: ${ANTI_AFF_VM1_PORT1_NAME} exists."
    fi

    tmp_id=$(openstack port show ${ANTI_AFF_VM2_PORT1_NAME} -f value -c id  2> /dev/null)
    if [[ -z "${tmp_id// }" ]]; then
        run_cmd "openstack port create --network ${PERF_NET_NAME} ${ANTI_AFF_VM2_PORT1_NAME}"
    else
        echo "WARN: ${ANTI_AFF_VM2_PORT1_NAME} exists."
    fi
}

# *****************************
# set_my_ostack_ids
# *****************************
set_my_ostack_ids() {
    export MY_OS_NET_ID_PERF=$(openstack network show ${PERF_NET_NAME} -f value -c id 2> /dev/null)
    export MY_OS_SRG_AFF_ID=$(openstack server group list -f value 2> /dev/null | grep -v anti | grep ${SGP_NAME_AFFINITY} | awk '{print $1}')
    export MY_OS_SRG_ANTI_AFF_ID=$(openstack server group list -f value 2> /dev/null | grep ${SGP_NAME_ANTI_AFF} | awk '{print $1}')

    export AFF_VM1_PORT1_ID=$(openstack port show ${AFF_VM1_PORT1_NAME} -f value -c id  2> /dev/null)
    export AFF_VM2_PORT1_ID=$(openstack port show ${AFF_VM2_PORT1_NAME} -f value -c id  2> /dev/null)
    export ANTI_AFF_VM1_PORT1_ID=$(openstack port show ${ANTI_AFF_VM1_PORT1_NAME} -f value -c id  2> /dev/null)
    export ANTI_AFF_VM2_PORT1_ID=$(openstack port show ${ANTI_AFF_VM2_PORT1_NAME} -f value -c id  2> /dev/null)

    export AFF_VM1_PORT1_IP=$(openstack port show ${AFF_VM2_PORT1_NAME} -f value -c fixed_ips  2> /dev/null | grep "ip_address='[0-9]*\." | cut -d"'" -f2)
    export AFF_VM2_PORT1_IP=$(openstack port show ${SRIOV_AFFI_DPORT2_NAME} -f value -c fixed_ips  2> /dev/null | grep "ip_address='[0-9]*\." | cut -d"'" -f2)
    export ANTI_AFF_VM1_PORT1_ID=$(openstack port show ${ANTI_AFF_VM1_PORT1_NAME} -f value -c fixed_ips  2> /dev/null | grep "ip_address='[0-9]*\." | cut -d"'" -f2)
    export ANTI_AFF_VM1_PORT1_ID=$(openstack port show ${ANTI_AFF_VM2_PORT1_NAME} -f value -c fixed_ips  2> /dev/null | grep "ip_address='[0-9]*\." | cut -d"'" -f2)
}

# *****************************
# set_my_ostack_ids
# *****************************
create_affi_groups() {
    MY_OS_SRG_AFF_ID=$(openstack server group list -f value 2> /dev/null | grep -v anti | grep ${SGP_NAME_AFFINITY} | awk '{print $1}')
    if [[ ! -z "${MY_OS_SRG_AFF_ID// }" ]]; then
       echo "WARN: ${SGP_NAME_AFFINITY} exists with id ${MY_OS_SRG_AFF_ID}. "
    else
        echo "Create server group ${SGP_NAME_AFFINITY} : starting..."
        CMD="openstack server group create --policy affinity ${SGP_NAME_AFFINITY}"
        run_cmd "${CMD}"
        echo "Create server group ${SGP_NAME_AFFINITY} : done"
    fi

    MY_OS_SRG_ANTI_AFF_ID=$(openstack server group list -f value 2> /dev/null | grep ${SGP_NAME_ANTI_AFF} | awk '{print $1}')
    if [[ ! -z "${MY_OS_SRG_ANTI_AFF_ID// }" ]]; then
       echo "WARN: ${SGP_NAME_ANTI_AFF} exists with id ${MY_OS_SRG_ANTI_AFF_ID}. "
    else
        echo "Create server group ${SGP_NAME_ANTI_AFF} : starting..."
        CMD="openstack server group create --policy anti-affinity ${SGP_NAME_ANTI_AFF}"
        run_cmd "${CMD}"
        echo "Create server group ${SGP_NAME_ANTI_AFF} : done"
    fi
}

# *****************************
# check_exists_vm_by_name
# *****************************
check_exists_vm_by_name() {
    local L_VM_NAME="${1}"

    TMP_ID=$(openstack server list --name "${L_VM_NAME}" -f value -c ID 2> /dev/null)
    if [[ ! -z "${TMP_ID// }" ]]; then
       echo "ERROR: VM ${L_VM_NAME} exists with id ${TMP_ID}. "
       exit 2
    fi

    return 0
}

PREP_CREATE_IPERF_VMS_DONE=0
# *****************************
# prep_create_iperf_vms
# *****************************
prep_create_iperf_vms() {
    if [[ ${PREP_CREATE_IPERF_VMS_DONE:-} -eq 1 ]]; then
        return
    fi
    set_my_ostack_ids
    if [[ -z "${MY_OS_NET_ID_PERF// }" ]]; then
       echo "ERROR: ${PERF_NET_NAME} PERF_NET_NAME is invalid. Check configuration at ${MY_CONF_FILE}"
       exit 2
    fi
    if [[ -z "${MY_OS_SRG_AFF_ID// }" ]]; then
       echo "ERROR: ${SGP_NAME_AFFINITY} SGP_NAME_AFFINITY is invalid. Check configuration at ${MY_CONF_FILE}"
       exit 2
    fi
    if [[ -z "${MY_OS_SRG_ANTI_AFF_ID// }" ]]; then
       echo "ERROR: ${SGP_NAME_ANTI_AFF} SGP_NAME_ANTI_AFF is invalid. Check configuration at ${MY_CONF_FILE}"
       exit 2
    fi
    PREP_CREATE_IPERF_VMS_DONE=1
}

# *****************************
# create_iperf_vm
# *****************************
create_iperf_vm() {
     local L_VM_NAME="$1"
     local L_SRG_ID="$2"
     local L_FLAVOR="${3:-${VM_FLAVOR}}"
     local L_IMAGE="${4:-${VM_IMG_NAME}}"
     local L_NET_ID="${5:-${MY_OS_NET_ID_PERF}}"
     local L_AZONE="${5:-${VM_AVAILIBILITY_ZONE}}"
     local L_USER_DATA_FILE="${6:-${VM_USERDATA}}"

     check_exists_vm_by_name "${L_VM_NAME}"

     CMD="openstack server create \
--key-name default --flavor ${L_FLAVOR} \
--image ${L_IMAGE} \
--nic net-id=${L_NET_ID} \
--user-data ${L_USER_DATA_FILE} \
--availability-zone ${L_AZONE} \
--hint group=${L_SRG_ID} \
--wait \
${L_VM_NAME}"
    run_cmd "${CMD}"

    #openstack server show "${L_VM_NAME}"

    #L_FID=$(openstack floating ip create ${MY_PUBLIC_NET} -f value -c id 2> /dev/null)
    #L_FIP=$(openstack floating ip show  ${L_FID} -f value -c floating_ip_address 2> /dev/null)
    #echo "FloatingIpId=${L_FIP},  FloatingIpId=${L_FID}"
    #CMD="openstack server add floating ip ${L_VM_NAME} ${L_FIP}"
    #run_cmd "${CMD}"
}

# *****************************
# create_iperf_affi_vms
# *****************************
create_iperf_affi_vms() {
    echo "Create iperf affinity VMs : starting..."
    prep_create_iperf_vms
    create_iperf_vm "${AFF_VM1_NAME}" "${MY_OS_SRG_AFF_ID}"
    create_iperf_vm "${AFF_VM2_NAME}" "${MY_OS_SRG_AFF_ID}"
    echo "Create iperf affinity VMs : done."
}

# *****************************
# create_iperf_anti_affi_vms
# *****************************
create_iperf_anti_affi_vms() {
    echo "Create iperf anti-affinity VMs : starting..."
    prep_create_iperf_vms
    create_iperf_vm "${ANTI_AFF_VM1_NAME}" "${MY_OS_SRG_ANTI_AFF_ID}"
    create_iperf_vm "${ANTI_AFF_VM2_NAME}" "${MY_OS_SRG_ANTI_AFF_ID}"
    echo "Create iperf anti-affinity VMs : done."
}

# *****************************
# create_iperf_test_vms
# *****************************
create_iperf_test_vms() {
    echo "Create iperf VMs : starting..."
    prep_create_iperf_vms
    create_iperf_affi_vms
    create_iperf_anti_affi_vms
    echo "Create iperf VMs : done."
}

# *****************************
# delete_iperf_anti_affi_vms
# *****************************
delete_iperf_anti_affi_vms() {
    echo "Delete iperf anti-affinity VMs : starting..."
    prep_create_iperf_vms
    CMD="openstack server delete ${ANTI_AFF_VM1_NAME}"
    run_cmd "${CMD}"
    CMD="openstack server delete ${ANTI_AFF_VM2_NAME}"
    run_cmd "${CMD}"
    echo "Delete iperf anti-affinity VMs : done."
}

# *****************************
# delete_iperf_affi_vms
# *****************************
delete_iperf_affi_vms() {
    echo "Delete iperf affinity VMs : starting..."
    prep_create_iperf_vms
    CMD="openstack server delete ${AFF_VM1_NAME}"
    run_cmd "${CMD}"
    CMD="openstack server delete ${AFF_VM2_NAME}"
    run_cmd "${CMD}"
    echo "Delete iperf affinity VMs : done."
}

# *****************************
# delete_iperf_test_vms
# *****************************
delete_iperf_test_vms() {
    echo "Delete iperf VMs : starting..."
    delete_iperf_affi_vms
    delete_iperf_anti_affi_vms
    echo "Delete iperf VMs : done."
}


case "$OPER" in
   init-profile)
        init_profile "${PRM02}"
        ;;
   create-net-ports)
        create_net_ports
        ;;
   create-affi-groups)
        create_affi_groups
        ;;
   create-iperf-test-vms)
        create_iperf_test_vms
        ;;
   create-iperf-test-vms)
        create_iperf_test_vms
        ;;
   create-iperf-affi-vms)
        create_iperf_affi_vms
        ;;
   create-iperf-anti-affi-vms)
        create_iperf_anti_affi_vms
        ;;
   create-iperf-test-vms)
        create_iperf_test_vms
        ;;
   delete-iperf-test-vms)
        delete_iperf_test_vms
        ;;
   delete-iperf-affi-vms)
        delete_iperf_affi_vms
        ;;
   delete-iperf-anti-affi-vms)
        delete_iperf_anti_affi_vms
        ;;
   *)
        usage
        ;;
esac
