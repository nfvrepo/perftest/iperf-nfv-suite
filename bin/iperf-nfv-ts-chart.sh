#!/bin/bash
########################################################################################
# File           : iperf-nfv-ts-chart.sh
# Version        : 0.5.0
# Author(s)      : Erol Ozcan <https://gitlab.com/erol.ozcan> (Argela)
# Create date    : 2018-06-02
# Description    : 
#########################################################################################
TMP_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TMP_SCRIPT_FNAME=$(basename $0)
# shopt -s nullglob
# PDATA_FILES=${1:-"result/*.pdata"}
OPER="${1}"
PRM02="${2}"
PRM03="${3}"
PRM04="${4}"
PRM05="${5}"
PRM06="${6}"

[[ -e bash-util-common-lib.sh ]] && source bash-util-common-lib.sh || source ${TMP_SCRIPT_DIR}/bash-util-common-lib.sh
[[ -e bash-util-common-lib.sh ]] && source iperf-nfv-ts-lib.sh || source ${TMP_SCRIPT_DIR}/iperf-nfv-ts-lib.sh

# PDATA field orders
#declare -R PDATA_F_TESTNAME=1

######################
# gen_ts_svg_bw
######################
gen_ts_svg_bw() {
    # expected file extension .pdata
    local l_pdata_file="${1}"

    if [[ -z "${l_pdata_file// }" ]]; then
       echo "Usage: $0 gen-ts-svg-bw [pdata-file]"
       exit 1
    fi
    if [[ ! -e "${l_pdata_file}" ]]; then
       echo "ERROR: ${l_pdata_file} pdata file does not exists!"
       exit 2
    fi

    echo "l_pdata_file=$l_pdata_file, l_svg_file=$l_svg_file"
    local l_pdata_file_noext=$(basename "$l_pdata_file" .pdata)
    local l_pdata_dir=$(dirname $l_pdata_file)
    local l_info_file="${l_pdata_dir}/${l_pdata_file_noext}.info"
    local l_svg_file1="${l_pdata_dir}/${l_pdata_file_noext}-bw.svg"
    local l_jpg_file1="${l_pdata_dir}/${l_pdata_file_noext}-bw.jpg"
    local l_svg_file2=""
    local l_jpg_file2=""
    echo "l_pdata_file=$l_pdata_file, l_svg_file=$l_svg_file, l_info_file=$l_info_file"
    local l_plot_cmd_tmp=""
    local l_plot_cmd=""
    local l_jpeg_quality="40%"

    (
    . "${l_info_file}"
    local l_plot_header=$(cat << ARGEOF
set term svg size 1920,1080 fname "Helvetica Neue" fsize 18 rounded dashed
set title  "${MAIN_TITLE}\n${TITLE}" font ",25"
set grid xtics ytics mytics
#set border 1
set autoscale y
#set key spacing 1.2
set style fill solid 1.00 border -1
set style histogram cluster gap 1 title textcolor lt -1
set style data histograms
set boxwidth 0.8
#set xtic scale 0 font ",8" rotate by 90
#set xtic rotate by 90
set key top left
set xtic border scale 1,0 font ",18" nomirror autojustify rotate by 270
# norangelimit
#unset ytics

# Convert bytes to megabytes
#set format y '%.0s%cB'
set format y "%.0f"
#set ylabel "MBits/sec"
#set yrange [0:10000]
#set ytics 50

set datafile separator "\t"

#set key off auto columnheader
#set yrange [0:*]
ARGEOF
)

    local l_plot_cmd1=""
    local l_plot_cmd2=""
    if [[ ${UDP_ENABLED} -eq 0 ]]; then
        l_plot_cmd1=$(cat << ARGEOF
set ylabel "MBits/sec"

#set offset 0,0,graph 0.05,0
# dx = 1/(2 * (numberOfColData + gap))
dx = 1/8.0

plot "${l_pdata_file}" using 2:xtic(1) title "Client", '' using 3 title "Server", \
     '' using (\$0 - dx):2:2 notitle with labels offset -1,0.5 ,\
     '' using (\$0 + dx):3:3 notitle with labels offset 1,0.5
ARGEOF
)
        l_svg_file2="${l_pdata_dir}/${l_pdata_file_noext}-retransmit.svg"
        l_jpg_file2="${l_pdata_dir}/${l_pdata_file_noext}-retransmit.jpg"
        l_plot_cmd2=$(cat << ARGEOF
set ylabel "Packages"

#set offset 0,0,graph 0.05,0
# dx = 1/(2 * (numberOfColData + gap))
dx = 1/6.0

plot "${l_pdata_file}" using 12:xtic(1) title "Sent Retransmits", \
     '' using (\$0 - dx):12:12 notitle with labels offset 0,0.8
ARGEOF
)
    else
        l_plot_cmd1=$(cat << ARGEOF
set ylabel "MBits/sec"

#set offset 0,0,graph 0.05,0
# dx = 1/(2 * (numberOfColData + gap))
dx = 1/6.0

plot "${l_pdata_file}" using 2:xtic(1) title "Client", \
     '' using (\$0 - dx):2:2 notitle with labels offset 0,0.8
ARGEOF
)
        l_svg_file2="${l_pdata_dir}/${l_pdata_file_noext}-lostper.svg"
        l_jpg_file2="${l_pdata_dir}/${l_pdata_file_noext}-lostper.jpg"
        l_plot_cmd2=$(cat << ARGEOF
set ylabel "Percentages"

#set offset 0,0,graph 0.05,0
# dx = 1/(2 * (numberOfColData + gap))
dx = 1/4.0

plot "${l_pdata_file}" using 14:xtic(1) title "Lost Percentage", \
     '' using (\$0 - dx):14:14 notitle with labels offset 0,0.8
ARGEOF
)

    fi

    cat << ARGEOF | gnuplot > $l_svg_file1
${l_plot_header}
${l_plot_cmd1}
ARGEOF
    echo "File $l_svg_file1 is generated."

    convert "$l_svg_file1" -quality "${l_jpeg_quality}" "$l_jpg_file1"
    echo "File $l_jpg_file1 is generated."

    cat << ARGEOF | gnuplot > $l_svg_file2
${l_plot_header}
${l_plot_cmd2}
ARGEOF
    echo "File $l_svg_file2 is generated."

    convert "$l_svg_file2" -quality "${l_jpeg_quality}" "$l_jpg_file2"
    echo "File $l_jpg_file2 is generated."
    )
}

######################
# gen_ts_svg_cpuutil
######################
gen_ts_svg_cpuutil() {
    local l_pdata_file="${1}"

    if [[ -z "${l_pdata_file// }" ]]; then
       echo "Usage: $0 gen-ts-svg-cpu-util [pdata-file]"
       exit 1
    fi
    if [[ ! -e "${l_pdata_file}" ]]; then
       echo "ERROR: ${l_pdata_file} pdata file does not exists!"
       exit 2
    fi
    
    echo "l_pdata_file=$l_pdata_file, l_svg_file=$l_svg_file"
    local l_pdata_file_noext=$(basename "$l_pdata_file" .pdata)
    local l_pdata_dir=$(dirname $l_pdata_file)
    local l_info_file="${l_pdata_dir}/${l_pdata_file_noext}.info"
    local l_svg_file="${l_pdata_dir}/${l_pdata_file_noext}-cpu.svg"
    local l_jpg_file="${l_pdata_dir}/${l_pdata_file_noext}-cpu.jpg"
    echo "l_pdata_file=$l_pdata_file, l_svg_file=$l_svg_file, l_info_file=$l_info_file"
    local l_jpeg_quality="40%"

    (
    . "${l_info_file}"

    local l_plot_cmd1=$(cat << ARGEOF
#unset log
#unset label
set term svg size 1920,1080 fname "Helvetica Neue" fsize 18 rounded dashed
set title  "${MAIN_TITLE}\n${TITLE}" font ",25"
set grid xtics ytics mytics
#set border 1
set autoscale y
#set key spacing 1.2
set style fill solid 1.00 border -1
set style histogram cluster gap 1 title textcolor lt -1
set style data histograms
set boxwidth 0.8
#set xtic scale 0 font ",8" rotate by 90
#set xtic rotate by 90
set key top left
set xtic border scale 1,0 font ",18" nomirror autojustify rotate by 270
# norangelimit
#unset ytics

# Convert bytes to megabytes
#set format y '%.0s%cB'
set format y "%.0f"
set ylabel "CPU Util Percentage"
#set yrange [0:100]
#set ytics 50

#set key off auto columnheader
#set yrange [0:*]
#set offset 0,0,graph 0.05,0
# dx = 1/(2 * (numberOfColData + gap))
dx = 1/6.0

set datafile separator "\t"
plot "${l_pdata_file}" using 4:xtic(1) title "Client", '' using 5 title "Server", \
     '' using (\$0 - dx):4:4 notitle with labels offset -0.5,0.5 ,\
     '' using (\$0 + dx):5:5 notitle with labels offset 0.5,0.5
ARGEOF
)
    cat << ARGEOF | gnuplot > $l_svg_file
${l_plot_cmd1}
ARGEOF
    echo "File $l_svg_file is generated."

    convert "$l_svg_file" -quality "${l_jpeg_quality}" "$l_jpg_file"
    echo "File $l_jpg_file is generated."
    )

}

# *****************************
# gen_ts_svg_all
# *****************************
gen_ts_svg_all() {
    local l_pdata_file="${1}"

    gen_ts_svg_bw "${l_pdata_file}"
    gen_ts_svg_cpuutil "${l_pdata_file}"
}

# *****************************
# gen_all_ts_svgs
# *****************************
gen_all_ts_svgs() {
    local l_pdata_dir="${1}"

    if [[ -z "${l_pdata_dir// }" ]]; then
       echo "Usage: $0 gen-all-ts-svgs [pdata-dir]"
       exit 1
    fi
    if [[ ! -e "${l_pdata_dir}" ]]; then
       echo "ERROR: ${l_pdata_dir} pdata dir does not exists!"
       exit 2
    fi

    local l_pdata_file=""
    for l_pdata_file in ${l_pdata_dir}/*.pdata; do
       gen_ts_svg_all "${l_pdata_file}"
    done
}

# *****************************
# usage
# *****************************
usage() {
    cat << ARGEOF
Usage: $0 <gen-ts-svg-bw|gen-ts-svg-cpu-util|gen-ts-svg-all|gen-all-ts-svgs> [other_params]
ARGEOF
}

case "$OPER" in
   gen-ts-svg-bw)
        # PRM02: pdata_file
        gen_ts_svg_bw "${PRM02}"
        ;;
   gen-ts-svg-cpu-util)
        # PRM02: pdata_file
        gen_ts_svg_cpuutil "${PRM02}"
        ;;
   gen-ts-svg-all)
        # PRM02: pdata_file
        gen_ts_svg_bw "${PRM02}"
        gen_ts_svg_cpuutil "${PRM02}"
        ;;
   gen-all-ts-svgs)
        # PRM02: directory of pdata_files
        gen_all_ts_svgs ${PRM02}
        ;;
   *)
        usage
        ;;
esac
