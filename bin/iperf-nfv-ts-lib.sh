#!/usr/bin/env bash
########################################################################################
# File           : iperf-nfv-ts-lib.sh
# Version        : 0.5.0
# Author(s)      : Erol Ozcan <https://gitlab.com/erol.ozcan> (Argela)
# Create date    : 2018-06-25
# Description    : iPerf NFV TestSuite bash library.
#########################################################################################
[[ ${IPERF_NFV_TS_LIB_LOADED:-} -eq 1 ]] && return || readonly -p IPERF_NFV_TS_LIB_LOADED=1

TMP_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

DEV_TEST_MODE=${DEV_TEST_MODE:-"off"}
[[ "${DEV_TEST_MODE// }" == "on" ]] && declare -r CONF_EXT="conf.sample" || declare -r CONF_EXT="conf"

PERFTEST_HOME=${PERFTEST_HOME:-"/argela/perftest"}
INTS_DIST_DIR=${INTS_DIST_DIR:-"${PERFTEST_HOME}/dist"}

declare -rx IPERF_NFV_TS_VERSION="0.5.0"
# directories
declare -rx IPERF_NFV_TS_HOME="${PERFTEST_HOME}/iperf-nfv-ts"
declare -rx INTS_ROOT_DIR="${IPERF_NFV_TS_HOME}"
declare -rx INTS_BACKUP_DIR="${INTS_ROOT_DIR}/backup"
declare -rx INTS_BIN_DIR="${IPERF_NFV_TS_HOME}/bin"
declare -rx INTS_ETC_DIR="${IPERF_NFV_TS_HOME}/etc"
declare -rx INTS_RESULT_DIR="${INTS_ROOT_DIR}/result"
# common files
declare -rx INTS_INIT_FILENAME="init.info"
declare -rx INTS_INIT_FILE="${INTS_ETC_DIR}/${INTS_INIT_FILENAME}"
declare -rx PROFILE_INFO_FILENAME="profile.info"
declare -rx OSTACK_COMMON_CONF_FILENAME="ostack-common.conf"
declare -rx OSTACK_COMMON_CONF_FILE="${INTS_ETC_DIR}/${OSTACK_COMMON_CONF_FILENAME}"
declare -rx OSTACK_CONF_FILENAME="ostack.conf"
declare -rx OSTACK_VMS_CONF_FILENAME="ostack-vms.conf"
declare -rx COMMON_CONF_FILENAME="iperf-nfv-ts-common.conf"
declare -rx COMMON_CONFIG_FILE="${INTS_ETC_DIR}/${COMMON_CONF_FILENAME}"
declare -rx TS_GROUP_CONF_FILENAME="testsuite-group.conf"
declare -rx INTS_CONF_FILENAME="iperf-nfv-ts.conf"
declare -rx IPERF_NFV_TS_CLIENT_SH="iperf-nfv-ts-client.sh"
declare -rx IPERF_NFV_TS_MNGR_SH="iperf-nfv-ts-manager.sh"

declare -rx INIT_RUN_ONCE_FUNC_NAME="run_once_at_init"
# init states
declare -rx INIT_STATE_NOT_SET="not_set"
declare -rx INIT_STATE_STARTED="started"
declare -rx INIT_STATE_FAILED="failed"
declare -rx INIT_STATE_DONE="done"

let "ONE_MBIT = 1000 * 1000"

source ${TMP_SCRIPT_DIR}/bash-util-common-lib.sh

######################
# print banner
######################
print_banner() {
    cat << ARGEOF
-----------------------------------------------------------------------
iPerf NFV Test suite v${IPERF_NFV_TS_VERSION}

INTS_ROOT_DIR   : ${INTS_ROOT_DIR}
INTS_BACKUP_DIR : ${INTS_BACKUP_DIR}
INTS_BIN_DIR    : ${INTS_BIN_DIR}
INTS_ETC_DIR    : ${INTS_ETC_DIR}
INTS_RESULT_DIR : ${INTS_RESULT_DIR}
-----------------------------------------------------------------------
ARGEOF
}

######################
# ints_mkdirs
######################
ints_mkdirs() {
    mkdir -p "${INTS_BACKUP_DIR}" "${INTS_ETC_DIR}" "${INTS_RESULT_DIR}"
}

######################
# check_profile
######################
check_profile() {
    local l_profile="${1}"

    if [[ -z "${l_profile// }" ]]; then
       echo "Usage: $0 ${OPER} [profile]"
       exit 1
    fi
    if [[ ! -d "${INTS_ETC_DIR}/${l_profile}" ]]; then
       echo "ERROR: ${l_profile} profile does not exists!"
       exit 2
    fi
    if [[ ! -f "${INTS_ETC_DIR}/${l_profile}/${PROFILE_INFO_FILENAME}" ]]; then
       echo "ERROR: ${l_profile} profile ${PROFILE_INFO_FILENAME} info does not exists!"
       exit 2
    fi
}


######################
# source_common_confs
######################
source_common_confs() {
    echo "DEBUG: source_common_confs ..."
    . "${INTS_INIT_FILE}"
    . "${OSTACK_COMMON_CONF_FILE}"
    . "${COMMON_CONFIG_FILE}"
    echo "DEBUG: source_common_confs done."
}

######################
# source_prf_default_confs
######################
source_prf_default_confs() {
    local l_profile="${1}"

    echo "DEBUG: source_prf_default_confs ${l_profile}..."
    source_common_confs

    . "${INTS_ETC_DIR}/${l_profile}/${PROFILE_INFO_FILENAME}"
    local l_src_file=""
    for l_src_file in "${INTS_PROFILE_SOURCE_ORDER[@]}"; do
        . "${INTS_ETC_DIR}/${l_profile}/${l_src_file}"
    done

    echo "DEBUG: source_prf_default_confs ${l_profile} done."
}



######################
# check_profile
######################
read_ts_config() {
    local l_profile="${1}"
    local l_conf_filename="${2}"

    if [[ -z "${l_conf_filename// }" ]]; then
       echo "Usage: $0 [profile] [conf-filename]"
       exit 1
    fi
    [[ ! ${l_conf_filename} == *.${CONF_EXT} ]] && l_conf_filename="${l_conf_filename}.${CONF_EXT}"

    local l_conf_file="${INTS_ETC_DIR}/${l_profile}/${l_conf_filename}"
    if [[ ! -e "${l_conf_file}" ]]; then
       echo "ERROR: $0 ${l_conf_file} config file does not exists!"
       exit 2
    fi

    echo "DEBUG: read_ts_config ts_conf_file ${l_conf_file}"
    source_prf_default_confs ${l_profile}
    . "${l_conf_file}"
    echo "DEBUG: read_ts_config  ${l_conf_file} done."
}

######################
# read_tsg_config
######################
read_tsg_config() {
    local l_profile="${1}"

    local l_tsg_file="${INTS_ETC_DIR}/${l_profile}/${TS_GROUP_CONF_FILENAME}"
    if [[ ! -e "${l_tsg_file}" ]]; then
       echo "ERROR: Testsuite group config ${l_tsg_file} does not exists!"
       exit 1
    fi

    . "${l_tsg_file}"
}

######################
# prepare_tsg_summary_info
######################
prepare_tsg_summary_info() {
    local l_profile="${1}"
    local l_tsg_name=${2:-"all"}
    local l_ts_name=${3:-"all"}

    local l_tsg_list_arrname=${4:-g_all_tsg_list}
    local l_ts_list_arrname=${5:-g_all_testsuite_list}
    local l_conf_files_arrname=${6:-g_all_conf_files}
    local l_client_ips_arrname=${7:-g_all_client_ips}
    local l_client_mgmt_ipunames_arrname=${8:-g_all_client_mgmt_ipunames}
    local l_server_ips_arrname=${9:-g_all_server_ips}
    local l_server_mgmt_ipunames_arrname=${10:-g_all_server_mgmt_ipunames}

    read_tsg_config "${l_profile}"

    eval "declare -n l_tsg_list_arr=${l_tsg_list_arrname}"
    eval "declare -n l_ts_list_arr=${l_ts_list_arrname}"
    eval "declare -n l_conf_files_arr=${l_conf_files_arrname}"
    eval "declare -n l_client_ips_arr=${l_client_ips_arrname}"
    eval "declare -n l_client_mgmt_ipunames_arr=${l_client_mgmt_ipunames_arrname}"
    eval "declare -n l_server_ips_arr=${l_server_ips_arrname}"
    eval "declare -n l_server_mgmt_ipunames_arr=${l_server_mgmt_ipunames_arrname}"

    # clean up global arrays
    eval "unset ${l_tsg_list_arrname}"
    eval "unset ${l_ts_list_arrname}"
    eval "unset ${l_conf_files_arrname}"
    eval "unset ${l_client_ips_arrname}"
    eval "unset ${l_client_mgmt_ipunames_arrname}"
    eval "unset ${l_server_ips_arrname}"
    eval "unset ${l_server_mgmt_ipunames_arrname}"

    declare -a l_tmp_ts_list=()
    declare -a l_tmp_conf_files=()
    declare -a l_tmp_client_ips=()
    declare -a l_tmp_client_mgmt_ips=()
    declare -a l_tmp_server_ips=()
    local l_tmp_ipuname=""
    local l_tmp_found_ts_name=0
    local l_tmp_ts_name=""
    local l_tmp_mgmt_client_ip=""

    #echo "DEBUG: profile=${l_profile}, tsg_name=${l_tsg_name}, ts_name=${l_ts_name}"
    for l_tsg_assocarr_name in "${ALL_TSG_VAR_NAMES[@]}"; do
        eval "declare -n l_tsg_assoc_arr=${l_tsg_assocarr_name}"
        #  && "${l_tsg_name}" -ne "${l_tsg_assoc_arr[NAME]}"
        [[ "${l_tsg_name}" != "all" && "${l_tsg_name}" != "${l_tsg_assoc_arr[NAME]}" ]] && continue

        l_tmp_found_ts_name=0
        IFS='|' read -a l_tmp_ts_list <<< "${l_tsg_assoc_arr[TESTSUITE_LIST]}"
        for l_tmp_ts_name in "${l_tmp_ts_list[@]}"; do
            [[ "${l_ts_name}" != "all" && "${l_ts_name}" != "${l_tmp_ts_name}" ]] && continue
            #echo "DEBUG:   l_tmp_ts_name=${l_tmp_ts_name}"
            l_tmp_found_ts_name=1
            break
        done
        [[ "${l_tmp_found_ts_name}" -ne "1" ]] && continue

        [[ ! -z "${l_ts_name// }" && "${l_ts_name}" != "all" ]] &&  l_tmp_ts_list=("${l_ts_name}")

        l_tsg_list_arr+=("${l_tsg_assoc_arr[NAME]}")

        IFS='|' read -a l_tmp_client_mgmt_ips <<< "${l_tsg_assoc_arr[CLIENT_MGMT_IPS]}"
        for l_tmp_mgmt_client_ip in "${l_tmp_client_mgmt_ips[@]}"; do
            l_tmp_ipuname="${l_tsg_assoc_arr[CLIENT_USERNAME]}@${l_tmp_mgmt_client_ip}"
            elem_exists_in_array "${l_tmp_ipuname}" "${l_client_mgmt_ipunames_arr[@]}"
            local l_rc=$?
            if [[ "${l_rc}" -ne "0" ]]; then
                l_client_mgmt_ipunames_arr+=("${l_tmp_ipuname}")
            fi
        done

        IFS='|' read -a l_tmp_client_ips <<< "${l_tsg_assoc_arr[CLIENT_IPS]}"
        for l_tmp_client_ip in "${l_tmp_client_ips[@]}"; do
            elem_exists_in_array "${l_tmp_client_ip}" "${l_client_ips_arr[@]}"
            local l_rc=$?
            if [[ "${l_rc}" -ne "0" ]]; then
                l_client_ips_arr+=("${l_tmp_client_ip}")
            fi
        done

        for l_tmp_ts_name in "${l_tmp_ts_list[@]}"; do
            [[ "${l_ts_name}" != "all" && "${l_ts_name}" != "${l_tmp_ts_name}" ]] && continue

            l_ts_list_arr+=("${l_tmp_ts_name}")
            l_tmp_conf_filename="${l_tmp_ts_name}.${CONF_EXT}"
            l_conf_files_arr+=("${INTS_ETC_DIR}/${l_profile}/${l_tmp_conf_filename}")

            read_ts_config "${l_profile}" "${l_tmp_conf_filename}"

            elem_exists_in_array "${SERVER_IP}" "${l_server_ips_arr[@]}"
            local l_rc=$?
            if [[ "${l_rc}" -ne "0" ]]; then
                l_server_ips_arr+=("${SERVER_IP}")
            fi

            l_tmp_ipuname="${SERVER_USERNAME}@${SERVER_MGMT_IP}"
            elem_exists_in_array "${l_tmp_ipuname}" "${l_server_mgmt_ipunames_arr[@]}"
            local l_rc=$?
            if [[ "${l_rc}" -ne "0" ]]; then
                l_server_mgmt_ipunames_arr+=("${l_tmp_ipuname}")
            fi
        done
    done
}

######################
# tsg_summary_info_test
#
#tsg_summary_info_test "sriov" "ext-srv"
#tsg_summary_info_test "sriov" "affinity"
#tsg_summary_info_test "sriov" "anti-affinity"
######################
tsg_summary_info_test() {
    local l_profile="${1}"
    local l_tsg_name=${2:-"all"}
    local l_ts_name=${3:-"all"}

    check_profile "${l_profile}"

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_ipunames

    prepare_tsg_summary_info "${l_profile}" "${l_tsg_name}" "${l_ts_name}" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_ipunames"

    echo "DEBUG: START - tsg_summary_info"
    for l_tsg_name in "${g_all_tsg_list[@]}"; do echo "DEBUG: tsg name ${l_tsg_name}"; done;
    for l_ts_name in "${g_all_testsuite_list[@]}"; do echo "DEBUG: ts_name ${l_ts_name}"; done;
    for l_conf_file in "${g_all_conf_files[@]}"; do echo "DEBUG: conf file ${l_conf_file}"; done;
    for l_client_ip in "${g_all_client_ips[@]}"; do echo "DEBUG: client ip ${l_client_ip}"; done;
    for l_client_ip_ipuname in "${g_all_client_mgmt_ipunames[@]}"; do echo "DEBUG: client mgmt ipuname ${l_client_ip_ipuname}"; done;
    for l_server_ip in "${g_all_server_ips[@]}"; do echo "DEBUG: server ip ${l_server_ip}"; done;
    for l_server_mgmt_ipuname in "${g_all_server_ipunames[@]}"; do echo "DEBUG: server  mgmt ipuname ${l_server_mgmt_ipuname}"; done;
    echo "DEBUG: FINISH - tsg_summary_info"
}


######################
# find_tsg_var_map
######################
find_tsg_var_map() {
    local l_profile="${1}"
    local l_tsg_name="${2}"
    local l_assoc_array_name="${3}"

    read_tsg_config "${l_profile}"

    local l_tsg_varname=${TSG_NAME_VAR_MAP["${l_tsg_name}"]}
    if [[ -z "${l_tsg_varname// }" ]]; then
       echo "ERROR: tsg_name ${l_tsg_name} is invalid!"
       exit 1
    fi
    local l_assoc_array_string=$(declare -p ${l_tsg_varname})
    eval "declare -gA ${l_assoc_array_name}=${l_assoc_array_string#*=}"
}

######################
# prepare_tsg_vars
######################
prepare_tsg_vars() {
    local l_profile="${1}"
    local l_tsg_name="${2}"

    local l_tsg_assocarr_name=${3:-g_tsg_assoc_arr}
    local l_ts_list_arrname=${4:-g_tsg_testsuite_list}
    local l_conf_files_arrname=${5:-g_tsg_arr_conf_files}
    local l_client_ips_arrname=${6:-g_tsg_client_ips}
    local l_server_ips_arrname=${7:-g_tsg_server_ips}

    check_profile "${l_profile}"
    find_tsg_var_map ${l_profile} ${l_tsg_name} "${l_tsg_assocarr_name}"

    eval "declare -n l_tsg_assoc_arr=${l_tsg_assocarr_name}"
    IFS='|' read -a "${l_ts_list_arrname}" <<< "${l_tsg_assoc_arr[TESTSUITE_LIST]}"
    IFS='|' read -a "${l_client_ips_arrname}" <<< "${l_tsg_assoc_arr[CLIENT_IPS]}"

    local l_ts_conf_file=""
    eval "declare -n l_ts_list=${l_ts_list_arrname}"
    eval "declare -n l_server_ips=${l_server_ips_arrname}"
    eval "declare -n l_conf_files=${l_conf_files_arrname}"

    for l_ts_name in "${l_ts_list[@]}"; do
        l_ts_conf_file="${l_ts_name}.${CONF_EXT}"
        l_conf_files+=("${INTS_ETC_DIR}/${l_profile}/${l_ts_conf_file}")

        read_ts_config "${l_profile}" "${l_ts_name}"

        echo "DEBUG: prepare_tsg_vars l_server_ips ${SERVER_IP}"
        elem_exists_in_array "${SERVER_IP}" "${l_server_ips[@]}"
        local l_rc=$?
        if [[ "${l_rc}" -ne "0" ]]; then
            l_server_ips+=("${SERVER_IP}")
        fi

    done
    #for l_ip in "${g_tmp_server_ips[@]}"; do echo "DEBUG: ip zzx $l_ip"; done;
}

######################
# get_server_screen_name
######################
get_server_screen_name() {
    local l_profile="${1}"
    local l_tsg_name="${2}"

    if [[ -z "${l_profile// }" ]]; then
       echo "ERROR: l_profile ${l_profile} is invalid!"
       exit 1
    fi
    if [[ -z "${l_tsg_name// }" ]]; then
       echo "ERROR: tsg_name ${l_tsg_name} is invalid!"
       exit 1
    fi

    echo "${l_profile}_${l_tsg_name}"

}

#############################
# write_sys_info_cmd_header
#############################
write_sys_info_cmd_header() {
    local l_out_file="${1}"
    local l_cmd="${2}"

    cat << ARGEOF >> "${l_out_file}"
# *****************************************************************************
# START command : ${l_cmd}
# output        :
ARGEOF

}


#############################
# write_sys_info_cmd_footer
#############################
write_sys_info_cmd_footer() {
    local l_out_file="${1}"
    local l_cmd="${2}"

    cat << ARGEOF >> "${l_out_file}"
# FINISH command : ${l_cmd}
# *****************************************************************************
ARGEOF

}

######################
# write_sys_info
######################
write_sys_info() {
    local l_out_filename=${1}
    local l_nic_name=${2:-"eth0"}

    if [[ -z "${l_out_filename// }" ]]; then
       echo "Usage: $0 [out filename] [tested NIC name]"
       exit 1
    fi

    if [[ -z "${l_nic_name// }" ]]; then
       echo "Usage: $0 [out filename] [tested NIC name]"
       exit 1
    fi

    local l_out_file="${INTS_RESULT_DIR}/${l_out_filename}"
    run_cmd "touch ${l_out_file}"
    local l_rc=$?
    if [[ "${l_rc}" -ne "0" ]]; then
        echo "ERROR: Cannot update out file ${l_out_file} ."
    fi

    local l_info_cmd_list=("hostname && hostname -I ")
    l_info_cmd_list+=("ip addr show dev ${l_nic_name}")
    l_info_cmd_list+=("ip link show dev ${l_nic_name}")
    l_info_cmd_list+=("ifconfig ${l_nic_name}")
    l_info_cmd_list+=("ethtool -i ${l_nic_name}")
    l_info_cmd_list+=("ethtool -a ${l_nic_name}")
    l_info_cmd_list+=("ethtool -c ${l_nic_name}")
    l_info_cmd_list+=("ethtool -g ${l_nic_name}")
    l_info_cmd_list+=("ethtool -k ${l_nic_name}")
    l_info_cmd_list+=("lscpu")
    l_info_cmd_list+=("free -m")
    l_info_cmd_list+=("sysctl -a 2> /dev/null  | grep -E '(rmem_|wmem_|optmem_max|netdev_budget|netdev_max_b|udp_mem|tcp_.mem)'")
    l_info_cmd_list+=("lsmod")
    l_info_cmd_list+=("lspci -nn")

    local l_cmd=""

    for l_cmd in "${l_info_cmd_list[@]}"; do
        write_sys_info_cmd_header "${l_out_file}" "${l_cmd}"
        run_cmd "${l_cmd} >> ${l_out_file} 2>&1"
        write_sys_info_cmd_footer "${l_out_file}" "${l_cmd}"
    done
}

######################
# set_tpl_file_to_config
######################
set_tpl_file_to_config() {
    local l_tpl_file=$1

    local l_out_file=${l_tpl_file%.tpl}
    l_out_file=${l_out_file/tpl/etc}
    local l_out_dir=$(dirname ${l_out_file})
    if [[ ! -d ${l_out_dir} ]]; then
        mkdir -p $(dirname ${l_out_file})
    fi

    echo "INFO: START set config ${l_out_file} from tpl ${l_tpl_file}"

    local l_tpl_content=$(<${l_tpl_file})
    local l_tmp_vars=( $(printf "%s" "${l_tpl_content}" | grep -E '^#@@(.*)@@' | sed -e 's/^#@@\(.*\)@@/\1/') )
    #echo "DEBUG: ${l_tmp_vars}"
    local l_tmp_var=""
    local l_tmp_pat=""
    for l_tmp_var in ${l_tmp_vars[@]}; do
        l_tmp_pat="^.*${l_tmp_var}=.*"
        if ! [[ ${l_tpl_content} =~ ${l_tmp_pat} ]]; then
            echo "WARN: variable ${l_tmp_var} not found in ${l_tpl_file}."
            continue
        fi
        # check array
        local l_tmp_val=$(printf "%s" "${l_tpl_content}" | grep -E "^declare -ga ${l_tmp_var}=" | cut -d"=" -f2)
        if [[ ! -z "${l_tmp_val// }" ]]; then
            # array
            echo -n "${l_tmp_var} (default: ${l_tmp_val[@]}) : "
            local l_tmp_arr=
            read -a l_tmp_arr
            if [[ ${#l_tmp_arr[@]} -lt 1 ]]; then
                eval "l_tmp_arr=${l_tmp_val[@]}"
            fi
            local tmp_sed_pat="s/^(declare -ga )${l_tmp_var}=.*/\1${l_tmp_var}=\(${l_tmp_arr[@]}\)/"
            #echo "DEBUG:  ${l_tmp_arr[@]} tmp_sed_pat=${tmp_sed_pat}"
            l_tpl_content=$(printf "%s" "${l_tpl_content}" | sed -E "${tmp_sed_pat}" )
        else
            l_tmp_val=$(printf "%s" "${l_tpl_content}" | grep -E "^(export )?${l_tmp_var}=" | cut -d"=" -f2)
            local l_tmp_input=""
            read -p "${l_tmp_var} (default: ${l_tmp_val}) : " l_tmp_input
            if [[ -z "${l_tmp_input// }" ]]; then
                l_tmp_input="${l_tmp_val}"
            fi
            #echo "DEBUG: l_tmp_input=${l_tmp_input}"
            l_tpl_content=$(printf "%s" "${l_tpl_content}" | sed -E "s~^(export )?${l_tmp_var}=.*~\1${l_tmp_var}=${l_tmp_input}~")
        fi
    done
    if [[ ${l_out_file} == *.${CONF_EXT} || ${l_out_file} == *.info ]]; then
        echo "# Generated from template ${l_tpl_file} at $(echo_date_fmt)" > ${l_out_file}
    fi
    echo "${l_tpl_content}" > ${l_out_file}

    # TODO think about override environment variables for the next coming templates
    local l_run_init_func=$(printf "%s" "${l_tpl_content}" | grep -E "^${INIT_RUN_ONCE_FUNC_NAME}\(\)")
    if [[ ! -z "${l_run_init_func// }" ]]; then
        . ${l_out_file}
        eval "${INIT_RUN_ONCE_FUNC_NAME}"
        echo "DEBUG: set config ${l_out_file} ${AFF_VM1_PORT1_NAME} - ${AFF_VM1_PORT1_ID} - ${AFF_VM1_PORT1_IP}"
    fi

    echo "INFO: FINISHED set config ${l_out_file} from tpl ${l_tpl_file}"
}

######################
# ints_check_init
######################
ints_check_init() {
    local l_quiet=${1:-"1"}

    if [[ ! -e ${INTS_INIT_FILE} ]]; then
        [[ "${l_quiet}" == "1" ]] || echo "ERROR: init not called yet!"
        return -1
    fi

    . ${INTS_INIT_FILE}

    case "${INTS_INIT_STATE}" in
       "${INIT_STATE_DONE}")
            [[ "${l_quiet}" == "1" ]] || echo "DEBUG: init state is ${INTS_INIT_STATE}"
            ;;
        *)
            [[ "${l_quiet}" == "1" ]] || echo "ERROR: init state is ${INTS_INIT_STATE}"
            return -3
            ;;
    esac

    return 0
}

######################
# ints_init_common
######################
ints_init_common() {
    echo "INFO: START ints_init_common"

    ints_mkdirs

    set_tpl_file_to_config "tpl/${INTS_INIT_FILENAME}.tpl"

    local l_tmp_date=$(echo_date_fmt)
    sed -i "/INTS_INIT_STATE=/c\INTS_INIT_STATE=${INIT_STATE_STARTED}" ${INTS_INIT_FILE}
    sed -i "/INTS_INIT_START=/c\INTS_INIT_START=${l_tmp_date}" ${INTS_INIT_FILE}

    local -a l_init_files=("tpl/${OSTACK_COMMON_CONF_FILENAME}.tpl" "tpl/${COMMON_CONF_FILENAME}.tpl")
    local l_init_file=""
    for l_init_file in "${l_init_files[@]}"; do
        set_tpl_file_to_config ${l_init_file}
    done

    l_tmp_date=$(echo_date_fmt)
    sed -i "/INTS_INIT_STATE=/c\INTS_INIT_STATE=${INIT_STATE_DONE}" ${INTS_INIT_FILE}
    sed -i "/INTS_INIT_FINISH=/c\INTS_INIT_FINISH=${l_tmp_date}" ${INTS_INIT_FILE}
    echo "INFO: FINISHED ints_init_common"
}

######################
# ints_do_inplace_replacement
######################
ints_do_inplace_replacement() {
    local l_file=$1

    local l_content=$(<"${l_file}")
    local -a l_tmp_vars=( $(printf "%s" "${l_content}" | grep -E '^.*%%(.*)%%.*' | sed -e 's/^.*%%\(.*\)%%.*/\1/') )
    local l_tmp_vars_str=${l_tmp_vars[*]}
    #echo "DEBUG: l_tmp_vars_str ${l_tmp_vars_str}"
    [[ -z "${l_tmp_vars_str// }" ]] && return 0
    local l_tmp_size=${#l_tmp_vars[@]}
    #echo "DEBUG: l_tmp_vars ${l_tmp_vars[@]} size=${l_tmp_size}"
    [[ ${l_tmp_size} -lt 1 ]] && return 0

    local l_tmp_var=""
    local l_tmp_val=""
    for l_tmp_var in "${l_tmp_vars[@]}"; do
        l_tmp_val="${!l_tmp_var}"
        echo "DEBUG: ints_do_inplace_replacement l_tmp_var ${l_tmp_var}=${l_tmp_val}"
        [[ -z "${l_tmp_val}" ]] && continue

        l_content=$(printf "%s" "${l_content}" | sed -E "s~^(.*)(%%${l_tmp_var}%%)(.*)~\1${l_tmp_val}\3~")
    done
    printf "%s" "${l_content}" | head -1 > ${l_file}
    echo "# updated at $(echo_date_fmt)" >> ${l_file}
    printf "%s\n" "${l_content}" | tail -n +2 >> ${l_file}
}

######################
# ints_init_profile
######################
ints_init_profile() {
    local l_profile=$1
    echo "INFO: START ints_init_profile ${l_profile}"

    if [[ -z "${l_profile}" ]]; then
        echo "ERROR: invalid profile ${l_profile}"
        exit 1
    fi

    source_common_confs

    mkdir -p "${INTS_ETC_DIR}/${l_profile}"
    set_tpl_file_to_config "tpl/${l_profile}/${PROFILE_INFO_FILENAME}.tpl"

    local l_prfl_info_file="etc/${l_profile}/${PROFILE_INFO_FILENAME}"
    . ${l_prfl_info_file}
    local l_tmp_date=$(echo_date_fmt)
    sed -i "/INTS_PROFILE_STATE=/c\INTS_PROFILE_STATE=${INIT_STATE_STARTED}" ${l_prfl_info_file}
    sed -i "/INTS_PROFILE_START=/c\INTS_PROFILE_START=${l_tmp_date}" ${l_prfl_info_file}

    # first init test cases depended configs
    local l_tpl_file=""
    for l_tpl_file in "${INTS_PROFILE_INIT_FILES[@]}"; do
        set_tpl_file_to_config "tpl/${l_profile}/${l_tpl_file}"
    done

    # source profile and do in-place replacement for the required variables
    echo "INFO: START source profile and do in-place replacement for the required variables."
    local l_src_file=""
    for l_src_file in "${INTS_PROFILE_INIT_SRC_ORDER[@]}"; do
        ints_do_inplace_replacement "${INTS_ETC_DIR}/${l_profile}/${l_src_file}"
        . "${INTS_ETC_DIR}/${l_profile}/${l_src_file}"
    done
    local l_rpl_file=""
    for l_rpl_file in "${INTS_PROFILE_INIT_TPL_VAR_REPLACE[@]}"; do
        ints_do_inplace_replacement "${INTS_ETC_DIR}/${l_profile}/${l_rpl_file}"
    done
    echo "INFO: FINISH source profile and do in-place replacement for the required variables."

    echo "INFO: START set tpl file to config and do in-place replacement."
    local -a l_tpl_files=( $(ls tpl/${l_profile}/*.tpl) )
    local l_tpl_filename=""
    local l_tmp_filename=""
    l_tpl_file=""
    for l_tpl_file in "${l_tpl_files[@]}"; do
        l_tpl_filename=$(basename ${l_tpl_file})
        elem_exists_in_array "${l_tpl_filename}" "${INTS_PROFILE_INIT_FILES[@]}"
        local l_rc=$?
        [[ "${l_rc}" -eq "0" ]] && continue

        set_tpl_file_to_config ${l_tpl_file}
        l_tmp_filename=${l_tpl_filename%.tpl}
        ints_do_inplace_replacement "${INTS_ETC_DIR}/${l_profile}/${l_tmp_filename}"
    done
    echo "INFO: FINISH set tpl file to config and do in-place replacement."

    l_tmp_date=$(echo_date_fmt)
    sed -i "/INTS_PROFILE_STATE=/c\INTS_PROFILE_STATE=${INIT_STATE_DONE}" ${l_prfl_info_file}
    sed -i "/INTS_PROFILE_FINISH=/c\INTS_PROFILE_FINISH=${l_tmp_date}" ${l_prfl_info_file}
    echo "INFO: FINISHED ints_init_profile ${l_profile}"
}

######################
# ints_init_create_vms
######################
ints_init_create_vms() {
    local l_profile=$1

    source_prf_default_confs $l_profile
    . "${OSTACK_RCFILE}"

    profile_create_vms
}

######################
# ints_delete_vms
######################
ints_delete_vms() {
    local l_profile=$1

    source_prf_default_confs $l_profile
    . "${OSTACK_RCFILE}"

    profile_delete_vms
}

######################
# ints_delete_ports
######################
ints_delete_ports() {
    local l_profile=$1

    source_prf_default_confs $l_profile
    . "${OSTACK_RCFILE}"

    profile_delete_ports
}

######################
# iperf_bandwith_calc
######################
iperf_bandwith_calc() {
    local l_total_bw=$1
    local l_parallel_stream=$2

    let  "l_tbw_num_len=${#l_total_bw} - 1"
    local l_tbw_char=${l_total_bw: -1}
    local l_tbw_num=${l_total_bw:0:${l_tbw_num_len}}

    local l_result="$(bc <<< "scale=2; (${l_tbw_num}/${l_parallel_stream})*.993")${l_tbw_char}"
    echo ${l_result}
}


t1() {
    local l_tpl_file="tpl/ostack-common.conf.tpl"
    #l_tpl_file="tpl/iperf-nfv-ts-common.conf.tpl"

    set_tpl_file_to_config ${l_tpl_file}
}


#ints_init
#t1
#echo_date_fmt
#check_init
