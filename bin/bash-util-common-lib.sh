#!/usr/bin/env bash
########################################################################################
# File           : bash-util-common-lib.sh
# Version        : 0.5.0
# Author(s)      : Erol Ozcan <https://gitlab.com/erol.ozcan> (Argela)
# Create date    : 2018-05-02
# Description    : Bash common utility library script.
#########################################################################################
[[ ${BASH_COMMON_UTIL_LOADED:-} -eq 1 ]] && return || readonly BASH_COMMON_UTIL_LOADED=1

declare -r DATE_FMT="%Y%m%d%H%M%S"


######################
# echo_date_fmt
######################
echo_date_fmt() {
    echo $(date +${DATE_FMT})
}

######################
# array_join_elems
######################
array_join_elems() {
    local l_delim=${1}

    shift; echo -n "${1}"; shift
    printf "%s" "${@/#/$l_delim}"
}


###################################
# elem_exists_in_array
###################################
elem_exists_in_array() {
    local l_element=${1}
    local l_array=${@:2}

    local l_tmp_elem=
    for l_tmp_elem in ${l_array[@]} ; do
        #echo "DEBUG:   checking $l_element = $l_tmp_elem"
        if [[ "${l_element}" == "${l_tmp_elem}" ]] ; then
            return 0
        fi
    done

    return 1
}

###################################
# run_cmd
###################################
run_cmd() {
    local l_cmd="${1}"
    local l_start_dt=$(date +${DATE_FMT})

    echo "DEBUG: [$l_start_dt] Running command: ${l_cmd}"
    eval "${l_cmd}"
    local l_rc=$?
    local l_end_dt=$(date +${DATE_FMT})
    echo "DEBUG: [$l_end_dt] command finished. Return code: $l_rc"

    return ${l_rc}
}

###################################
# run_cmd_silent
###################################
run_cmd_silent() {
    local l_cmd="${1}"

    eval "${l_cmd}"
    local l_rc=$?
    return ${l_rc}
}


###################################
# get_listen_pid_by_program
###################################
get_listen_pid_by_program() {
    local l_program="${1}"
    local l_port=${1:-""}

    local l_port_grep=""
    [[ ! -z "${l_port// }" ]] && l_port_grep="| grep ${l_port}"

    local l_pid=$(eval "netstat -lutnp | grep ${l_program} ${l_port_grep}| awk '{ print \$7 }' | cut -d'/' -f1")
    echo "${l_pid}"
}