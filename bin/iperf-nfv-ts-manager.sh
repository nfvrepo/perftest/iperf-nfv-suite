#!/usr/bin/env bash
########################################################################################
# File           : iperf-nfv-ts-manager.sh
# Version        : 0.5.0
# Author(s)      : Erol Ozcan <https://gitlab.com/erol.ozcan> (Argela)
# Create date    : 2018-06-20
# Description    :
#########################################################################################
TMP_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
TMP_SCRIPT_FNAME=$(basename $0)

OPER="${1}"
PRM02="${2}"
PRM03="${3}"
PRM04="${4}"
PRM05="${5}"
PRM06="${6}"
PRM07="${7}"

shopt -s extglob

[[ -e bash-util-common-lib.sh ]] && source bash-util-common-lib.sh || source ${TMP_SCRIPT_DIR}/bash-util-common-lib.sh
[[ -e bash-util-common-lib.sh ]] && source iperf-nfv-ts-lib.sh || source ${TMP_SCRIPT_DIR}/iperf-nfv-ts-lib.sh

######################
# start_tsg_client_local
######################
start_tsg_client_local() {
    local l_profile="${1}"
    local l_tsg_name="${2}"

    check_profile "${l_profile}"
    declare -gA g_tsg_assoc_arr
    declare -ga g_tsg_testsuite_list g_tsg_client_ips g_tsg_server_ips
    prepare_tsg_vars "${l_profile}" "${l_tsg_name}" "g_tsg_assoc_arr" "g_tsg_testsuite_list" \
     "g_tsg_arr_conf_files" "g_tsg_client_ips" "g_tsg_server_ips"

    for l_ts_conf_file in ${g_tsg_arr_conf_files[@]}; do
       ${INTS_BIN_DIR}/iperf-nfv-ts-client.sh "run-testsuite" "${l_profile}" "${l_ts_conf_file//}"
    done
}

######################
# start_ts_client_local
######################
start_ts_client_local() {
    local l_profile="${1}"
    local l_ts_name="${2}"

    check_profile "${l_profile}"

    local l_ts_conf_file="${INTS_ETC_DIR}/${l_profile}/${l_ts_name}.${CONF_EXT}"
    if [[ ! -e "${l_ts_conf_file}" ]]; then
       echo "ERROR: Testsuite config ${l_ts_conf_file} does not exists!"
       exit 1
    fi

   ${INTS_BIN_DIR}/iperf-nfv-ts-client.sh "run-testsuite" "${l_profile}" "${l_ts_name}"
}

######################
# start_server_local
######################
start_server_local() {
    local l_profile="${1}"
    local l_tsg_name="${2}"
    local l_ts_name="${3}"
    local l_restart=${4:-"no"}

    check_profile "${l_profile}"

    if [[ -z "${l_tsg_name// }" ]]; then
       echo "Usage: $0 ${OPER} [profile] [tsg name] [ts name] [screen restart:yes/no]"
       exit 1
    fi

    if [[ -z "${l_ts_name// }" ]]; then
       echo "Usage: $0 ${OPER} [profile] [tsg name] [ts name] [screen restart:yes/no]"
       exit 1
    fi

    local l_tsg_assocarr_name="g_tsg_assoc_arr"
    find_tsg_var_map ${l_profile} ${l_tsg_name} "${l_tsg_assocarr_name}"

    read_ts_config "${l_profile}" "${l_ts_name}"

    local l_ts_start_date=$(date +${DATE_FMT})

    local l_ts_info_file="${RESULT_DIR}/${TESTSUITE_NAME}-${l_ts_start_date}.info"
    echo "DEBUG: l_ts_info_file=$l_ts_info_file"
    cat << ARGEOF > "${l_ts_info_file}"
MAIN_TITLE="${MAIN_TITLE}"
TESTSUITE_NAME="${TESTSUITE_NAME}"
TITLE="${TITLE}"
CLIENT_HOSTNAME="${CLIENT_HOSTNAME}"
CLIENT_TASKSET="${CLIENT_TASKSET}"
CLIENT_AFFINITY="${CLIENT_AFFINITY}"
SERVER_IP="${SERVER_IP}"
SERVER_MGMT_IP="${SERVER_MGMT_IP}"
SERVER_TASKSET="${SERVER_TASKSET}"
SERVER_AFFINITY="${SERVER_AFFINITY}"
BANDWITH="${BANDWITH}"
UDP_ENABLED=${UDP_ENABLED}
TIME=${TIME}
OMIT_TIME=${OMIT_TIME}
START_DATE="${l_ts_start_date}"
ARGEOF

    local l_sys_info_filename="${TESTSUITE_NAME}-${l_ts_start_date}.sysinfo"
    write_sys_info "${l_sys_info_filename}" "${SERVER_NIC}"

    local l_pid=$(get_listen_pid_by_program "iperf3" "${SERVER_PORT}")
    if [[ ! -z "${l_pid// }"  ]]; then
        if [[ "${l_restart// }" == "yes" ]]; then
            echo "WARN: killing existing iperf server pid ${l_pid}"
            run_cmd "echo ${l_pid} | xargs kill"
        else
            echo "WARN: ${l_profile} ${l_tsg_name} server exists with pid ${l_pid}, port ${SERVER_PORT}"
            return 0
        fi
    fi

    local l_screen_name=$(get_server_screen_name "${l_profile}" "${l_tsg_name}")

    # start iperf3 with screen
    # netstat -suna && nstat && nstat && taskset -c 6,8 iperf3 -s -V -4 -A 6 -B 192.168.131.254
    local l_pre_stat_cmd="netstat -suna && nstat && nstat &&"
    local l_pre_screen_cmd="screen -L -d -m -S \"${l_screen_name}\""
    local l_pre_taskset_cmd=""
    [[ ! -z "${SERVER_TASKSET// }" ]] && l_pre_taskset_cmd="taskset -c ${SERVER_TASKSET}"

    local l_prm_affinity=""
    [[ ! -z "${SERVER_AFFINITY// }" ]] && l_prm_affinity="-A ${SERVER_AFFINITY}"
    local l_prm_bind=""
    [[ ! -z "${SERVER_IP// }" ]] && l_prm_bind="-B ${SERVER_IP}"
    local l_iperf_server_cmd="iperf3 -s -V -4 ${l_prm_affinity} ${l_prm_bind}"

    local l_cmd="${l_pre_screen_cmd} bash -c \"${l_pre_stat_cmd} ${l_pre_taskset_cmd} ${l_iperf_server_cmd}\""
    run_cmd "${l_cmd}"
    sleep 0.5
    screen -ls | grep -q "${l_screen_name}"
    l_rc=$?
    if [[ "${l_rc}" -ne "0" ]]; then
        echo "ERROR: failed to start cmd: ${l_start_server_cmd}"
        exit 2
    fi

    return 0
}

######################
# stop_server_local
######################
stop_server_local() {
    local l_profile="${1}"
    local l_tsg_name="${2}"
    local l_ts_name="${3}"

    check_profile "${l_profile}"

    if [[ -z "${l_tsg_name// }" ]]; then
       echo "Usage: $0 ${OPER} [profile] [tsg name] [ts name]"
       exit 1
    fi

    if [[ -z "${l_ts_name// }" ]]; then
       echo "Usage: $0 ${OPER} [profile] [tsg name] [ts name]"
       exit 1
    fi

    local l_tsg_assocarr_name="g_tsg_assoc_arr"
    find_tsg_var_map ${l_profile} ${l_tsg_name} "${l_tsg_assocarr_name}"

    local l_screen_name=$(get_server_screen_name "${l_profile}" "${l_tsg_name}")
    local l_check_kill_cmd="screen -ls | grep \"${l_screen_name}\" | cut -d. -f1 | awk '{print \$1}' | xargs kill 2> /dev/null"
    run_cmd "${l_check_kill_cmd}"
    sleep 1

    read_ts_config "${l_profile}" "${l_ts_name}"

    netstat -lnp | grep ${SERVER_PORT} | grep -q iperf
    local l_rc=$?
    if [[ "${l_rc}" -eq "0" ]]; then
        echo "ERROR: iperf is still running!"
        return 1
    fi

    return 0
}

######################
# start_servers_remote
######################
start_servers_remote() {
    local l_profile="${1}"
    local l_tsg_name=${2}
    local l_ts_name=${3:-"all"}
    local l_restart=${4:-"no"}

    check_profile "${l_profile}"

    if [[ -z "${l_tsg_name// }" ]]; then
       echo "Usage: $0 ${OPER} [profile] [tsg name]"
       exit 1
    fi

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_ipunames

    prepare_tsg_summary_info "${l_profile}" "${l_tsg_name}" "${l_ts_name}" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_ipunames"

    local l_screen_name=$(get_server_screen_name "${l_profile}" "${l_tsg_name}")
    declare -a l_started_server_ips=()
    local l_tmp_ts_name=""
    for l_tmp_ts_name in "${g_all_testsuite_list[@]}"; do
        if [[ "${l_ts_name// }" != "all" && "${l_ts_name}" != "${l_tmp_ts_name}" ]]; then
            continue
        fi

        read_ts_config "${l_profile}" "${l_tmp_ts_name}"

        if [[ -z "${SERVER_MGMT_IP// }" ]]; then
          echo "WARN: SERVER_MGMT_IP is invalid, skipping ${l_ts_conf_file} .."
          continue
        fi

        elem_exists_in_array "${SERVER_MGMT_IP}" "${l_started_server_ips[@]}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "WARN: ${l_profile} ${l_tsg_name} server ${SERVER_MGMT_IP} has already been started."
            continue
        fi

        local l_ssh_connect_cmd="ssh ${SERVER_USERNAME}@${SERVER_MGMT_IP}"
        local l_remote_cmd="screen -ls | grep -q ${l_screen_name}"
        run_cmd "${l_ssh_connect_cmd} 'bash -c \"${l_remote_cmd}\"'"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "WARN: ${l_profile} ${l_tsg_name} server ${SERVER_MGMT_IP} has already been started."
            continue
        fi

        local l_ssh_connect_cmd="ssh ${SERVER_USERNAME}@${SERVER_MGMT_IP}"
        local l_remote_cmd="cd ${INTS_BIN_DIR} && ./${IPERF_NFV_TS_MNGR_SH} start-server-local ${l_profile} ${l_tsg_name} ${l_tmp_ts_name} ${l_restart}"
        run_cmd "${l_ssh_connect_cmd} 'bash -c \"${l_remote_cmd}\"'"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            l_started_server_ips+=("${SERVER_MGMT_IP}")
            echo "INFO: ${l_profile} ${l_tsg_name} server ${SERVER_MGMT_IP} started."
        else
            echo "ERROR: ${l_profile} ${l_tsg_name} server ${SERVER_MGMT_IP} cannot be started. rc=${l_rc}"
        fi
    done
}

######################
# stop_servers_remote
######################
stop_servers_remote() {
    local l_profile="${1}"
    local l_tsg_name=${2}
    local l_ts_name=${3:-"all"}

    check_profile "${l_profile}"

    if [[ -z "${l_tsg_name// }" ]]; then
       echo "Usage: $0 ${OPER} [profile] [tsg name]"
       exit 1
    fi

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_ipunames

    prepare_tsg_summary_info "${l_profile}" "${l_tsg_name}" "${l_ts_name}" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_ipunames"

    declare -a l_stop_server_ips=()
    local l_tmp_ts_name=""
    for l_tmp_ts_name in "${g_all_testsuite_list[@]}"; do
        if [[ "${l_ts_name// }" != "all" && "${l_ts_name}" != "${l_tmp_ts_name}" ]]; then
            continue
        fi

        read_ts_config "${l_profile}" "${l_tmp_ts_name}"

        if [[ -z "${SERVER_MGMT_IP// }" ]]; then
          echo "WARN: SERVER_MGMT_IP is invalid, skipping ${l_ts_conf_file} .."
          continue
        fi

        elem_exists_in_array "${SERVER_MGMT_IP}" "${l_stop_server_ips[@]}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "WARN: ${l_profile} ${l_tsg_name} server ${SERVER_MGMT_IP} has already been stopped."
            continue
        fi

        local l_ssh_connect_cmd="ssh ${SERVER_USERNAME}@${SERVER_MGMT_IP}"
        local l_remote_cmd="cd ${INTS_BIN_DIR} && ./${IPERF_NFV_TS_MNGR_SH} stop-server-local ${l_profile} ${l_tsg_name} ${l_tmp_ts_name}"
        run_cmd "${l_ssh_connect_cmd} 'bash -c \"${l_remote_cmd}\"'"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            l_stop_server_ips+=("${SERVER_MGMT_IP}")
            echo "INFO: ${l_profile} ${l_tsg_name} server ${SERVER_MGMT_IP} stopped."
        else
            echo "ERROR: ${l_profile} ${l_tsg_name} server ${SERVER_MGMT_IP} cannot be stopped. rc=${l_rc}"
        fi
    done
}

######################
# start_clients_remote
######################
start_clients_remote() {
    local l_profile="${1}"
    local l_tsg_name="${2}"
    local l_ts_name=${3:-"all"}

    check_profile "${l_profile}"

    if [[ -z "${l_tsg_name// }" ]]; then
       echo "Usage: $0 ${OPER} [profile] [tsg name] [ts name |default all]"
       exit 1
    fi

    echo "DEBUG: start_clients_remote params: ${l_profile} ${l_tsg_name} ${l_ts_name}"

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_ipunames

    prepare_tsg_summary_info "${l_profile}" "${l_tsg_name}" "${l_ts_name}" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_ipunames"

    declare -a l_started_server_ips=()
    local l_tmp_ts_name=""
    local l_client_ipuname=""
    for l_tmp_ts_name in "${g_all_testsuite_list[@]}"; do
        if [[ "${l_ts_name}" != "all" && "${l_ts_name}" != "${l_tmp_ts_name}" ]]; then
            continue
        fi
        for l_client_ipuname in "${g_all_client_mgmt_ipunames[@]}"; do
            local l_ssh_connect_cmd="ssh ${l_client_ipuname}"
            local l_remote_cmd="cd ${INTS_BIN_DIR} && ./${IPERF_NFV_TS_CLIENT_SH} run-testsuite ${l_profile} ${l_tmp_ts_name}"

            run_cmd "${l_ssh_connect_cmd} 'bash -c \"${l_remote_cmd}\"'"
            local l_rc=$?
            if [[ "${l_rc}" -eq "0" ]]; then
                echo "INFO: ${l_profile} ${l_tsg_name} client ${l_client_ip} started."
            else
                echo "ERROR: profile ${l_profile} tsg ${l_tsg_name} client ${l_client_ip} cannot be started. rc=${l_rc}"
            fi
        done
    done
}

######################
# start_all_clients_remote
######################
start_all_clients_remote() {
    local l_profile="${1}"

    check_profile "${l_profile}"

    read_tsg_config "${l_profile}"
    local l_ts_name="all"
    local l_tmp_tsg_name=""
    declare -a l_tmp_tsg_names
    # global TSG_NAME_VAR_MAP[@] usage in loop can cause unexpected problems!
    for l_tmp_tsg_name in "${!TSG_NAME_VAR_MAP[@]}"; do l_tmp_tsg_names+=("${l_tmp_tsg_name}"); done

    for l_tmp_tsg_name in "${l_tmp_tsg_names[@]}"; do
        echo "DEBUG: start_all_clients_remote ${l_tmp_tsg_name} ${l_ts_name}"
        start_clients_remote "${l_profile}" "${l_tmp_tsg_name}" "${l_ts_name}"
    done
}

######################
# start_all_servers_remote
######################
start_all_servers_remote() {
    local l_profile="${1}"
    local l_restart=${2:-"no"}

    check_profile "${l_profile}"

    read_tsg_config "${l_profile}"
    local l_tmp_tsg_name=""
    for l_tmp_tsg_name in "${!TSG_NAME_VAR_MAP[@]}"; do
        start_servers_remote "${l_profile}" "${l_tmp_tsg_name}" "all" "${l_restart}"
    done
}

######################
# stop_all_servers_remote
######################
stop_all_servers_remote() {
    local l_profile="${1}"

    check_profile "${l_profile}"

    read_tsg_config "${l_profile}"
    local l_tmp_tsg_name=""
    for l_tmp_tsg_name in "${!TSG_NAME_VAR_MAP[@]}"; do
        stop_servers_remote "${l_profile}" "${l_tmp_tsg_name}"
    done
}


######################
# prepare_tsg_vars_test
######################
prepare_tsg_vars_test() {
    local l_profile="${1}"
    local l_tsg_name="${2}"

    declare -gA g_tsg_assoc_arr
    declare -ga g_tsg_testsuite_list g_tsg_arr_conf_files g_tsg_client_ips g_tsg_server_ips
    prepare_tsg_vars "${l_profile}" "${l_tsg_name}" "g_tsg_assoc_arr" "g_tsg_testsuite_list" \
     "g_tsg_arr_conf_files" "g_tsg_client_ips" "g_tsg_server_ips"

    for l_tsg_ts_name in "${g_tsg_testsuite_list[@]}"; do echo "DEBUG: tsg ts_name ${l_tsg_ts_name}"; done;
    for l_conf_file in "${g_tsg_arr_conf_files[@]}"; do echo "DEBUG: conf file ${l_conf_file}"; done;
    for l_client_ip in "${g_tsg_client_ips[@]}"; do echo "DEBUG: client ip ${l_client_ip}"; done;
    for l_server_ip in "${g_tsg_server_ips[@]}"; do echo "DEBUG: server ip zzx ${l_server_ip}"; done;

}

######################
# sync_code_to_all
#  $1: profile
#  $2: target. valid targets: clients, servers, all
#  $3: scope. valid scopes: code, config, all
######################
sync_to_all() {
    local l_profile="${1}"
    local l_target=${2:-"all"}
    local l_scope=${3:-"all"}

    check_profile "${l_profile}"

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_mgmt_ipunames

    prepare_tsg_summary_info "${l_profile}" "all" "all" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_mgmt_ipunames"

    local l_ssh_cmd=""
    local l_scp_cmd=""
    local l_scp_src_pattern=""
    local l_rc=0
    case "${l_target}" in
        clients | all)
            declare -a l_client_sync_list=()
            for l_client_ipuname in "${g_all_client_mgmt_ipunames[@]}"; do
                elem_exists_in_array "${l_client_ipuname}" "${l_client_sync_list[@]}"
                l_rc=$?
                if [[ "${l_rc}" -eq "0" ]]; then
                    echo "WARN: ${l_profile} ${l_target} client ${l_client_ipuname} has already been processed."
                    continue
                fi

                case "${l_scope}" in
                    code) l_scp_src_pattern="-rp ${INTS_BIN_DIR}" ;;
                    config) l_scp_src_pattern="-rp ${INTS_ETC_DIR}" ;;
                    all) l_scp_src_pattern="-rp ${INTS_BIN_DIR} ${INTS_ETC_DIR}" ;;
                    *)
                        echo "ERROR: ${l_scope} is not valid scope."
                        exit 1
                        ;;
                esac
                # create root dir for the first time
                l_ssh_cmd="ssh ${l_client_ipuname} mkdir -p ${INTS_ROOT_DIR}"
                run_cmd "${l_ssh_cmd}"
                local l_rc=$?
                if [[ "${l_rc}" -ne "0" ]]; then
                    echo "ERROR: ${l_scope} mkdir ${INTS_ROOT_DIR} failed  @ ${l_client_ipuname} for ${l_profile}."
                    exit 2
                fi

                l_scp_cmd="scp ${l_scp_src_pattern} ${l_client_ipuname}:${INTS_ROOT_DIR}/"
                run_cmd "${l_scp_cmd}"
                local l_rc=$?
                if [[ "${l_rc}" -eq "0" ]]; then
                    echo "INFO: ${l_scope} copied to ${l_client_ipuname} for ${l_profile}."
                fi

                l_client_sync_list+=("${l_client_ipuname}")
            done
            ;;
        esac

    local l_server_ipuname=""
    case "${l_target}" in
        servers | all)
            #echo "DEBUG: server ..."
            declare -a l_server_sync_list=()
            for l_server_ipuname in "${g_all_server_mgmt_ipunames[@]}"; do
                elem_exists_in_array "${l_server_ipuname}" "${l_server_sync_list[@]}"
                l_rc=$?
                if [[ "${l_rc}" -eq "0" ]]; then
                    echo "WARN: ${l_profile} ${l_target} server ${l_server_ipuname} has already been processed."
                    continue
                fi

                echo "DEBUG: l_server_ipuname ${l_server_ipuname} ..."
                case "${l_scope}" in
                    code) l_scp_src_pattern="-rp ${INTS_BIN_DIR}" ;;
                    config) l_scp_src_pattern="-rp ${INTS_ETC_DIR}" ;;
                    all) l_scp_src_pattern="-rp ${INTS_BIN_DIR} ${INTS_ETC_DIR}" ;;
                    *)
                        echo "ERROR: ${l_scope} is not valid scope."
                        exit 1
                        ;;
                esac
                # create root dir for the first time
                l_ssh_cmd="ssh ${l_server_ipuname} mkdir -p ${INTS_ROOT_DIR}"
                run_cmd "${l_ssh_cmd}"
                local l_rc=$?
                if [[ "${l_rc}" -ne "0" ]]; then
                    echo "ERROR: ${l_scope} mkdir ${INTS_ROOT_DIR} failed  @ ${l_server_ipuname} for ${l_profile}."
                    exit 2
                fi

                l_scp_cmd="scp ${l_scp_src_pattern} ${l_server_ipuname}:${INTS_ROOT_DIR}/"
                run_cmd "${l_scp_cmd}"
                local l_rc=$?
                if [[ "${l_rc}" -eq "0" ]]; then
                    echo "INFO: ${l_scope} copied to ${l_server_ipuname} for ${l_profile}."
                fi

                l_server_sync_list+=("${l_server_ipuname}")
            done
            ;;
        *)
            echo "ERROR: target ${l_target} is not valid!"
            exit 1
    esac
}

######################
# sync_dist_to_all
#  $1: profile
#  $2: target. valid targets: clients, servers, all
######################
sync_dist_to_all() {
    local l_profile="${1}"
    local l_target=${2:-"all"}

    check_profile "${l_profile}"

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_mgmt_ipunames

    prepare_tsg_summary_info "${l_profile}" "all" "all" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_mgmt_ipunames"

    local l_ssh_cmd=""
    local l_src_pattern=""
    local l_rc=0
    case "${l_target}" in
        clients | all)
            declare -a l_client_sync_list=()
            for l_client_ipuname in "${g_all_client_mgmt_ipunames[@]}"; do
                elem_exists_in_array "${l_client_ipuname}" "${l_client_sync_list[@]}"
                l_rc=$?
                if [[ "${l_rc}" -eq "0" ]]; then
                    echo "WARN: ${l_profile} ${l_target} client ${l_client_ipuname} has already been processed."
                    continue
                fi

                local l_src_pattern="-rp ${INTS_DIST_DIR}"
                # create root dir for the first time
                l_ssh_cmd="ssh ${l_client_ipuname} mkdir -p ${PERFTEST_HOME}"
                run_cmd "${l_ssh_cmd}"
                l_rc=$?
                if [[ "${l_rc}" -ne "0" ]]; then
                    echo "ERROR: mkdir ${INTS_ROOT_DIR} failed  @ ${l_client_ipuname} for ${l_profile}."
                    exit 2
                fi

                l_ssh_cmd="scp ${l_src_pattern} ${l_client_ipuname}:${PERFTEST_HOME}/"
                run_cmd "${l_ssh_cmd}"
                l_rc=$?
                if [[ "${l_rc}" -eq "0" ]]; then
                    echo "INFO: ${INTS_DIST_DIR} copied to ${l_client_ipuname} for ${l_profile} clients."
                fi

                l_client_sync_list+=("${l_client_ipuname}")
            done
            ;;
        esac

    local l_server_ipuname=""
    case "${l_target}" in
        servers | all)
            #echo "DEBUG: server ..."
            declare -a l_server_sync_list=()
            for l_server_ipuname in "${g_all_server_mgmt_ipunames[@]}"; do
                elem_exists_in_array "${l_server_ipuname}" "${l_server_sync_list[@]}"
                l_rc=$?
                if [[ "${l_rc}" -eq "0" ]]; then
                    echo "WARN: ${l_profile} ${l_target} server ${l_server_ipuname} has already been processed."
                    continue
                fi

                #echo "DEBUG: l_server_ipuname ${l_server_ipuname} ..."
                local l_src_pattern="-rp ${INTS_DIST_DIR}"
                # create root dir for the first time
                l_ssh_cmd="ssh ${l_server_ipuname} mkdir -p ${PERFTEST_HOME}"
                run_cmd "${l_ssh_cmd}"
                local l_rc=$?
                if [[ "${l_rc}" -ne "0" ]]; then
                    echo "ERROR: mkdir ${INTS_ROOT_DIR} failed  @ ${l_server_ipuname} for ${l_profile}."
                    exit 2
                fi

                l_ssh_cmd="scp ${l_src_pattern} ${l_server_ipuname}:${PERFTEST_HOME}/"
                run_cmd "${l_ssh_cmd}"
                local l_rc=$?
                if [[ "${l_rc}" -eq "0" ]]; then
                   echo "INFO: ${INTS_DIST_DIR} copied to ${l_client_ipuname} for ${l_profile} servers."
                fi

                l_server_sync_list+=("${l_server_ipuname}")
            done
            ;;
        *)
            echo "ERROR: target ${l_target} is not valid!"
            exit 1
    esac
}

######################
# fetch_client_results
######################
fetch_client_results() {
    local l_profile="${1}"
    local l_tsg_name=${2:-"all"}
    local l_ts_name=${3:-"all"}

    check_profile "${l_profile}"

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_mgmt_ipunames

    prepare_tsg_summary_info "${l_profile}" "${l_tsg_name}" "${l_ts_name}" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_mgmt_ipunames"

    declare -a l_files_patterns
    local l_src_patterns=""
    if [[ "${l_tsg_name}" == "all" && "${l_ts_name}" == "all" ]]; then
        l_files_patterns=("*.*")
        l_src_patterns="--include='*'"
    else
        for l_tmp_ts_name in "${g_all_testsuite_list[@]}"; do
            l_files_patterns+=("${l_tmp_ts_name}*")
            l_src_patterns="${l_src_patterns} --include=${l_tmp_ts_name}*"
        done
    fi
    #local l_src_patterns=$(array_join_elems '--include ' "${l_files_patterns[@]}")
    # echo "DEBUG: l_patterns $l_patterns"
    for l_client_ipuname in "${g_all_client_mgmt_ipunames[@]}"; do
        #l_scp_cmd="scp -p ${l_client_ipuname}:${INTS_RESULT_DIR}/\{${l_src_patterns}\} ./result"
        l_scp_cmd="rsync -avh -e ssh ${l_src_patterns} --include='*/' --exclude='*' ${l_client_ipuname}:${INTS_RESULT_DIR}/ ${INTS_RESULT_DIR}"
        echo "DEBUG: l_scp_cmd ${l_scp_cmd}"
        run_cmd "${l_scp_cmd}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "INFO: ${l_files_pattern} copied to ${l_client_ipuname} for ${l_profile}."
        else
            echo "ERROR: ${l_files_pattern} cannot copied to ${l_client_ipuname} for ${l_profile}."
        fi
    done
}

######################
# backup_and_remove_client_results
######################
backup_and_remove_client_results() {
    local l_profile="${1}"
    local l_tsg_name=${2:-"all"}
    local l_ts_name=${3:-"all"}

    check_profile "${l_profile}"

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_mgmt_ipunames

    prepare_tsg_summary_info "${l_profile}" "${l_tsg_name}" "${l_ts_name}" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_mgmt_ipunames"

    declare -a l_files_patterns
    local l_src_patterns=""
    if [[ "${l_tsg_name}" == "all" && "${l_ts_name}" == "all" ]]; then
        l_files_patterns=("*.*")
    else
        for l_tmp_ts_name in "${g_all_testsuite_list[@]}"; do
            l_files_patterns+=("${l_tmp_ts_name}*")
        done
    fi
    #local l_src_patterns=$(array_join_elems '--include ' "${l_files_patterns[@]}")
    # echo "DEBUG: l_patterns $l_patterns"
    local l_tar_fn_ip_prefix=""
    local l_tar_src_patterns=""
    local l_tar_file_name=""
    local l_backup_date=""
    local l_scp_cmd=""
    for l_client_ipuname in "${g_all_client_mgmt_ipunames[@]}"; do
        l_tar_fn_ip_prefix="${l_client_ipuname#*@}"
        l_tar_fn_ip_prefix="${l_tar_fn_ip_prefix//./_}"
        l_backup_date=$(date +${DATE_FMT})
        l_tar_filename="${l_tar_fn_ip_prefix}-${l_tsg_name}-${l_backup_date}.tar.gz"
        l_tar_file="${INTS_BACKUP_DIR}/${l_tar_filename}"

        l_tar_src_patterns=$(printf '%s ' "${l_files_patterns[@]}")

        local l_ssh_connect_cmd="ssh ${l_client_ipuname}"
        local l_remote_cmd="mkdir -p ${INTS_BACKUP_DIR} && cd ${INTS_RESULT_DIR} && tar cvfz ${l_tar_file} ${l_tar_src_patterns}"
        l_scp_cmd="ssh ${l_client_ipuname} 'bash -c \"${l_remote_cmd}\"'"

        echo "DEBUG: l_scp_cmd ${l_scp_cmd}"
        run_cmd "${l_scp_cmd}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "INFO: ${l_profile} ${l_tsg_name} backup done. file ${l_client_ipuname}:${l_tar_file} "
        else
            echo "ERROR: ${l_profile} ${l_tsg_name} backup failed! file ${l_client_ipuname}:${l_tar_file} "
        fi

        l_scp_cmd="scp -p ${l_client_ipuname}:${l_tar_file} ${INTS_BACKUP_DIR}"
        echo "DEBUG: l_scp_cmd ${l_scp_cmd}"
        run_cmd "${l_scp_cmd}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "INFO: Backup file ${l_tar_file} copied to ${INTS_BACKUP_DIR}."
        else
            echo "ERROR: Backup file ${l_tar_file} cannot copied from ${l_client_ipuname}."
        fi

        l_remote_cmd="cd ${INTS_RESULT_DIR} && rm -f ${l_tar_src_patterns}"
        l_scp_cmd="ssh ${l_client_ipuname} 'bash -c \"${l_remote_cmd}\"'"
        echo "DEBUG: l_scp_cmd ${l_scp_cmd}"
        run_cmd "${l_scp_cmd}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "INFO: ${l_tar_src_patterns} results removed from ${INTS_RESULT_DIR}."
        else
            echo "ERROR: ${l_tar_src_patterns} results cannot removed from ${INTS_RESULT_DIR}."
        fi
    done
}

######################
# backup_and_remove_remote_results
######################
backup_and_remove_remote_results() {
    local l_profile="${1}"
    local l_tsg_name=${2:-"all"}
    local l_ts_name=${3:-"all"}

    check_profile "${l_profile}"

    declare -ga g_all_tsg_list
    declare -ga g_all_testsuite_list
    declare -ga g_all_conf_files
    declare -ga g_all_client_ips
    declare -ga g_all_client_mgmt_ipunames
    declare -ga g_all_server_ips
    declare -ga g_all_server_mgmt_ipunames

    prepare_tsg_summary_info "${l_profile}" "${l_tsg_name}" "${l_ts_name}" \
     "g_all_tsg_list" "g_all_testsuite_list" "g_all_conf_files" \
     "g_all_client_ips" "g_all_client_mgmt_ipunames" \
     "g_all_server_ips" "g_all_server_mgmt_ipunames"

    declare -a l_files_patterns
    local l_src_patterns=""
    if [[ "${l_tsg_name}" == "all" && "${l_ts_name}" == "all" ]]; then
        l_files_patterns=("*.*")
    else
        for l_tmp_ts_name in "${g_all_testsuite_list[@]}"; do
            l_files_patterns+=("${l_tmp_ts_name}*")
        done
    fi
    #local l_src_patterns=$(array_join_elems '--include ' "${l_files_patterns[@]}")
    # echo "DEBUG: l_patterns $l_patterns"
    local l_tar_fn_ip_prefix=""
    local l_tar_src_patterns=""
    local l_tar_file_name=""
    local l_backup_date=""
    local l_scp_cmd=""
    local l_old_ifs="$IFS"
    IFS=$'\n'
    local l_all_remote_ipunames=(`for tmp_ipn in "${g_all_client_mgmt_ipunames[@]}" "${g_all_server_mgmt_ipunames[@]}" ; do echo "$tmp_ipn" ; done | sort -du`)
    IFS="${l_old_ifs}"
    for l_rmt_ipuname in "${l_all_remote_ipunames[@]}"; do
        l_tar_fn_ip_prefix="${l_rmt_ipuname#*@}"
        l_tar_fn_ip_prefix="${l_tar_fn_ip_prefix//./_}"
        l_backup_date=$(date +${DATE_FMT})
        l_tar_filename="${l_tar_fn_ip_prefix}-${l_tsg_name}-${l_backup_date}.tar.gz"
        l_tar_file="${INTS_BACKUP_DIR}/${l_tar_filename}"

        l_tar_src_patterns=$(printf '%s ' "${l_files_patterns[@]}")

        local l_ssh_connect_cmd="ssh ${l_rmt_ipuname}"
        local l_remote_cmd="mkdir -p ${INTS_BACKUP_DIR} && cd ${INTS_RESULT_DIR} && tar cvfz ${l_tar_file} ${l_tar_src_patterns}"
        l_scp_cmd="ssh ${l_rmt_ipuname} 'bash -c \"${l_remote_cmd}\"'"

        echo "DEBUG: l_scp_cmd ${l_scp_cmd}"
        run_cmd "${l_scp_cmd}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "INFO: ${l_profile} ${l_tsg_name} backup done. file ${l_rmt_ipuname}:${l_tar_file} "
        else
            echo "ERROR: ${l_profile} ${l_tsg_name} backup failed! file ${l_rmt_ipuname}:${l_tar_file} "
        fi

        l_scp_cmd="scp -p ${l_rmt_ipuname}:${l_tar_file} ${INTS_BACKUP_DIR}"
        echo "DEBUG: l_scp_cmd ${l_scp_cmd}"
        run_cmd "${l_scp_cmd}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "INFO: Backup file ${l_tar_file} copied to ${INTS_BACKUP_DIR}."
        else
            echo "ERROR: Backup file ${l_tar_file} cannot copied from ${l_rmt_ipuname}."
        fi

        l_remote_cmd="cd ${INTS_RESULT_DIR} && rm -f ${l_tar_src_patterns}"
        l_scp_cmd="ssh ${l_rmt_ipuname} 'bash -c \"${l_remote_cmd}\"'"
        echo "DEBUG: l_scp_cmd ${l_scp_cmd}"
        run_cmd "${l_scp_cmd}"
        local l_rc=$?
        if [[ "${l_rc}" -eq "0" ]]; then
            echo "INFO: ${l_tar_src_patterns} results removed from ${INTS_RESULT_DIR}."
        else
            echo "ERROR: ${l_tar_src_patterns} results cannot removed from ${INTS_RESULT_DIR}."
        fi
    done
}



######################
# gen_all_charts_by_file_local
######################
gen_all_charts_by_file_local() {
    local l_pdata_file="${1}"

    if [[ ! -e "${l_pdata_file}" ]]; then
       echo "ERROR: pdata file ${l_pdata_file} does not exists!"
       exit 1
    fi

    echo "DEBUG: pdata file ${l_pdata_file}"
    ${INTS_BIN_DIR}/iperf-nfv-ts-chart.sh gen-ts-svg-all "${l_pdata_file}"
}

######################
# gen_all_charts_by_dir_local
######################
gen_all_charts_by_dir_local() {
    local l_result_dir=${1:-"${INTS_RESULT_DIR}"}

    if [[ ! -d "${l_result_dir}" ]]; then
       echo "ERROR: Result dir ${l_result_dir} does not exists!"
       exit 1
    fi

    local l_pdata_file=""
    for l_pdata_file in $(ls ${l_result_dir}/*.pdata); do
        gen_all_charts_by_file_local "${l_pdata_file}"
    done
}

######################
# gen_charts_by_pattern_local
######################
gen_charts_by_pattern_local() {
    local l_pdata_files_pattern=${@:1}

    if [[ -z "${l_pdata_files_pattern// }" ]]; then
       echo "Usage: $0 sgen_charts_by_pattern_local [profile] [pdata-files-pattern]"
       exit 1
    fi

    #read_tsg_config "${l_profile}"
    local l_pdata_file=""
    for l_pdata_file in ${l_pdata_files_pattern[@]}; do
        #local l_file_noext=$(basename "${l_pdata_file}" .pdata)
        #local l_test_date="${l_file_noext: -14}"
        #local l_test_name="${l_file_noext:0:${#l_file_noext}-15}"
        gen_all_charts_by_file_local "${l_pdata_file}"
    done

}

######################
# init_common
######################
init_common() {
    ints_init_common
}


######################
# init_profile
######################
init_profile() {
    local l_profile=$1

    if [[ -z "${l_profile// }" ]]; then
       echo "Usage: $0 init-profile [profile]"
       exit 1
    fi

    ints_init_profile ${l_profile}
}


######################
# init_create_vms
######################
init_create_vms() {
    local l_profile=$1

    check_profile "${l_profile}"

    ints_init_create_vms ${l_profile}
}


######################
# delete_vms
######################
delete_vms() {
    local l_profile=$1

    check_profile "${l_profile}"

    ints_delete_vms ${l_profile}
}

######################
# delete_ports
######################
delete_ports() {
    local l_profile=$1

    check_profile "${l_profile}"

    ints_delete_ports ${l_profile}
}


######################
# usage
######################
usage() {
    cat << ARGEOF
Usage: $0
  <init-common|init-profile|init-create-vms|delete-vms|delete-ports|update-common
   |sync-to-all|sync-dist-to-all
   |start-tsg-client-local| start-ts-client-local
   |start-server-local|start-servers-remote|start-all-servers-remote
   |stop-server-local|stop-servers-remote|stop-all-servers-remote
   |start-clients-remote|start-all-clients-remote
   |fetch-client-results
   |gen-all-charts-by-file-local|gen-all-charts-by-dir-local|gen-charts-by-pattern-local
   |backup-and-remove-client-results
   |write-sys-info>
  [other_params]
ARGEOF
}

print_banner
ints_mkdirs
if [[ ints_check_init -ne 0 ]]; then
    ints_check_init "0"
fi

case "$OPER" in
   init-common)
        init_common
        ;;
   init-profile)
        # PRM02: profile
        init_profile ${PRM02}
        ;;
   init-create-vms)
        # PRM02: profile
        init_create_vms ${PRM02}
        ;;
   delete-vms)
        # PRM02: profile
        delete_vms ${PRM02}
        ;;
   delete-ports)
        # PRM02: profile
        delete_ports ${PRM02}
        ;;
   sync-to-all)
        # PRM02: profile, PRM03: target, PRM04: scope
        sync_to_all ${PRM02} ${PRM03} ${PRM04}
        ;;
   sync-dist-to-all)
        # PRM02: profile, PRM03: target
        sync_dist_to_all ${PRM02} ${PRM03}
        ;;
   start-tsg-client-local)
        # PRM02: profile, PRM03: tsg
        start_tsg_client_local ${PRM02} ${PRM03}
        ;;
   start-ts-client-local)
        # PRM02: profile, PRM03: test suite name
        start_ts_client_local ${PRM02} ${PRM03}
        ;;
   start-server-local)
        # RM02: profile, PRM03: tsg, PRM04: ts, PRM05: server restart flag
        start_server_local ${PRM02} ${PRM03} ${PRM04} ${PRM05}
        ;;
   start-servers-remote)
        # PRM02: profile, PRM03: tsg, PRM04: ts_name, PRM05: server restart flag
        start_servers_remote ${PRM02} ${PRM03} ${PRM04} ${PRM05}
        ;;
   start-all-servers-remote)
        # PRM02: profile, server restart flag
        start_all_servers_remote ${PRM02} ${PRM03}
        ;;
   stop-server-local)
        # RM02: profile, PRM03: tsg, PRM04: ts
        stop_server_local ${PRM02} ${PRM03} ${PRM04}
        ;;
   stop-servers-remote)
        # PRM02: profile, PRM03: tsg, PRM04: ts_name
        stop_servers_remote ${PRM02} ${PRM03} ${PRM04}
        ;;
   stop-all-servers-remote)
        # PRM02: profile
        stop_all_servers_remote ${PRM02}
        ;;
   start-clients-remote)
        # PRM02: profile, PRM03: test suite name, PRM04: ts_name
        start_clients_remote ${PRM02} ${PRM03} ${PRM04}
        ;;
   start-all-clients-remote)
        # PRM02: profile, PRM03: force-restart (yes/no)
        start_all_clients_remote ${PRM02} ${PRM03}
        ;;
   fetch-client-results)
        # PRM02: profile, PRM03: test suite name, PRM04: ts_name
        fetch_client_results ${PRM02} ${PRM03} ${PRM04}
        ;;
   backup-and-remove-client-results)
        # PRM02: profile, PRM03: test suite name, PRM04: ts_name
        backup_and_remove_client_results ${PRM02} ${PRM03} ${PRM04}
        ;;
   backup-and-remove-remote-results)
        # PRM02: profile, PRM03: test suite name, PRM04: ts_name
        backup_and_remove_remote_results ${PRM02} ${PRM03} ${PRM04}
        ;;
   gen-all-charts-by-file-local)
        # PRM02: directory path
        gen_all_charts_by_file_local ${PRM02}
        ;;
   gen-all-charts-by-dir-local)
        # PRM02: directory path
        gen_all_charts_by_dir_local ${PRM02}
        ;;
   gen-charts-by-pattern-local)
        # PRM02: pdata pattern
        gen_charts_by_pattern_local ${PRM02}
        ;;
   prepare-tsg-vars-test)
        prepare_tsg_vars_test ${PRM02} ${PRM03}
        ;;
   tsg-summary-info-test)
        # PRM02: profile, PRM03: test suite name, PRM04: ts_name
        tsg_summary_info_test ${PRM02} ${PRM03} ${PRM04}
        ;;
   write-sys-info)
        # RM02: sys info filename, PRM03: tested NIC name
        write_sys_info ${PRM02} ${PRM03}
        ;;
   *)
        usage
        ;;
esac
