#!/usr/bin/env bash
########################################################################################
# File           : dl-install-iperf-nfv-ts.sh
# Version        : 0.5.0
# Author(s)      : Erol Ozcan <https://gitlab.com/erol.ozcan> (Argela)
# Create date    : 2018-07-25
# Description    : Download and install iperf-nfv-testsuite
########################################################################################
PERFTEST_DIR=${1:-"/argela/perftest"}

# url  https://gitlab.com/nfvrepo/perftest/iperf-nfv-suite/-/archive/master/iperf-nfv-suite-master.tar.gz
REPO_URL_ROOT="https://gitlab.com/nfvrepo/perftest"
PRJ_NAME="iperf-nfv-suite"
PRJ_VERSION="master"
PRJ_DIRNAME_WITH_VER="${PRJ_NAME}-${PRJ_VERSION}"
PRJ_ARCH_FILENAME="${PRJ_DIRNAME_WITH_VER}.tar.gz"
PRJ_ARCHIVE_URL="${REPO_URL_ROOT}/${PRJ_NAME}/-/archive/${PRJ_VERSION}/${PRJ_ARCH_FILENAME}"
PRJ_INSTALL_DIR="${PERFTEST_DIR}/${PROJECT_NAME}"

declare -r DATE_FMT="%Y%m%d%H%M%S"

######################
# print banner
######################
print_banner() {
    cat << ARGEOF
-----------------------------------------------------------------------
iPerf NFV Test Suite v${PRJ_VERSION} Download and Install

PRJ_ARCHIVE_URL : ${INTS_ROOT_PRJ_ARCHIVE_URLDIR}
PERFTEST_DIR    : ${PERFTEST_DIR}
-----------------------------------------------------------------------
ARGEOF
}

###################################
# run_cmd
###################################
run_cmd() {
    local l_cmd="${1}"
    local l_start_dt=$(date +${DATE_FMT})

    echo "DEBUG: [$l_start_dt] Running command: ${l_cmd}"
    eval "${l_cmd}"
    local l_rc=$?
    local l_end_dt=$(date +${DATE_FMT})
    echo "DEBUG: [$l_end_dt] command finished. Return code: $l_rc"

    return ${l_rc}
}


# *****************************
# os_version_check
# *****************************
os_version_check() {
    if [ -f /etc/os-release ]; then
        # freedesktop.org and systemd
        . /etc/os-release
        OS_NAME="${NAME}"
        OS_VER="${VERSION_ID}"
    elif type lsb_release >/dev/null 2>&1; then
        # linuxbase.org
        OS_NAME=$(lsb_release -si)
        OS_VER=$(lsb_release -sr)
    elif [ -f /etc/lsb-release ]; then
        # For some versions of Debian/Ubuntu without lsb_release command
        . /etc/lsb-release
        OS_NAME="${DISTRIB_ID}"
        OS_VER="${DISTRIB_RELEASE}"
    elif [ -f /etc/debian_version ]; then
        # Older Debian/Ubuntu/etc.
        OS_NAME="Debian"
        OS_VER=$(cat /etc/debian_version)
    else
        # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
        OS_NAME=$(uname -s)
        OS_VER=$(uname -r)
    fi
}

# *****************************
# install_pkgs_fedora
# *****************************
install_pkgs_fedora() {
    echo "Install required packages for Fedora: starting..."
    local l_cmd="yum update -y && yum install -y iperf3 jq nfs-utils screen"
    run_cmd "${l_cmd}"
    echo "Install required packages for Fedora: finished."
}

# *****************************
# install_pkgs_ubuntu
# *****************************
install_pkgs_ubuntu() {
    echo "Install required packages for Ubuntu : starting..."
    local l_cmd="apt-get update -y && apt-get install -y iperf3 jq nfs-utils screen"
    run_cmd  "${l_cmd}"
    echo "Install required packages for Ubuntu: finished."
}

# *****************************
# install_pkgs
# *****************************
install_pkgs() {
    local l_os_distro=$1

    case "${l_os_distro}" in
       Ubuntu)
            install_pkgs_ubuntu
            ;;
       Fedora)
            install_pkgs_fedora
            ;;
       *)
            usage
            ;;
    esac
}


######################
# dl_and_install
######################
dl_and_install() {
    echo "Downloading from ${PRJ_ARCHIVE_URL} ..."
    mkdir -p "${PERFTEST_DIR}"/{dist,tmp}
    cd ${PERFTEST_DIR}/dist && curl -O "${PRJ_ARCHIVE_URL}"

    echo "Extracting ${PRJ_ARCH_FILENAME} ..."
    cd ${PERFTEST_DIR} && tar xvfz ${PERFTEST_DIR}/dist/${PRJ_ARCH_FILENAME}
    ln -s "${PRJ_DIRNAME_WITH_VER}" "${PRJ_NAME}"

    install_pkgs "${OS_NAME}"
}

print_banner
os_version_check
dl_and_install
