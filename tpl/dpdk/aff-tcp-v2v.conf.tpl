#MAIN_TITLE="Turk Telekom RHOSP 12 Network Throughput Performance Tests"
TITLE="${INTS_PROFILE_TITLENAME} - Affinity TCP Traffic from VM to VM"
TESTSUITE_NAME="${INTS_PROFILE_SHORTNAME}-aff-tcp-v2v"
UDP_ENABLED=0
# server : Affinity VM1
SERVER_HOSTNAME="${AFF_VM1_NAME}"
SERVER_TASKSET="${AFF_VM1_TASKSET}"
SERVER_AFFINITY="${AFF_VM1_IPERF_AFF}"
SERVER_NIC="${AFF_VM1_PORT1_NIC}"
SERVER_IP="${AFF_VM1_PORT1_IP}"
SERVER_MGMT_NIC="${AFF_VM1_MGMT_NIC}"
SERVER_MGMT_IP="${AFF_VM1_MGMT_IP}"
# client : Affinity VM2
CLIENT_HOSTNAME="${AFF_VM2_NAME}"
CLIENT_TASKSET="${AFF_VM2_TASKSET}"
CLIENT_AFFINITY="${AFF_VM2_IPERF_AFF}"
CLIENT_NIC="${AFF_VM2_PORT1_NIC}"
CLIENT_IP="${AFF_VM2_PORT1_IP}"
#CLIENT_IP="$(ip addr show ${CLIENT_NIC} 2> /dev/null| grep 'inet ' | awk '{ print $2}' | cut -d'/' -f1)"
CLIENT_MGMT_IP="${AFF_VM2_MGMT_IP}"
