#cloud-config
# vim:syntax=yaml
debug: True
ssh_pwauth: True
disable_root: false
chpasswd:
  list: |
    root: iperf-nfv-ts
    fedora: iperf-nfv-ts
    centos: iperf-nfv-ts
    ubuntu: iperf-nfv-ts
  expire: false
runcmd:
#- sed -i'.orig' -e 's/without-password/yes/' /etc/ssh/sshd_config
- systemctl restart sshd.service
- cd /tmp && curl -O '%%IPERF_NFV_TS_DL_INST_URL%%'
- bash './dl-install-iperf-nfv-ts.sh'
