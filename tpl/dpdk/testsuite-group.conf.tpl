#!/usr/bin/env bash

declare -ga ALL_TSG_VAR_NAMES=("TSG_AFFINITY" "TSG_ANTI_AFFINITY" "TSG_EXT_SRV")
#declare -ga ALL_TSG_VAR_NAMES=("TSG_AFFINITY")

declare -gA TSG_NAME_VAR_MAP
TSG_NAME_VAR_MAP["affinity"]="TSG_AFFINITY"
TSG_NAME_VAR_MAP["anti-affinity"]="TSG_ANTI_AFFINITY"
TSG_NAME_VAR_MAP["ext-srv"]="TSG_EXT_SRV"
TSG_NAME_VAR_MAP["comp-node"]="TSG_COMP_NODE"

# affinity testsuite group
declare -gA TSG_AFFINITY
TSG_AFFINITY["NAME"]="affinity"
TSG_AFFINITY["TESTSUITE_LIST"]="aff-tcp-v2v|aff-udp-v2v"
TSG_AFFINITY["CLIENT_IPS"]="%%AFF_VM2_PORT1_IP%%"
TSG_AFFINITY["CLIENT_MGMT_IPS"]="%%AFF_VM2_MGMT_IP%%"
TSG_AFFINITY["CLIENT_USERNAME"]="root"

# anti-affinity testsuite group
declare -gA TSG_ANTI_AFFINITY
TSG_ANTI_AFFINITY["NAME"]="anti-affinity"
TSG_ANTI_AFFINITY["TESTSUITE_LIST"]="anti-aff-tcp-v2p|anti-aff-tcp-v2v|anti-aff-udp-v2p|anti-aff-udp-v2v"
#TSG_ANTI_AFFINITY["TESTSUITE_LIST"]="anti-aff-udp-v2p|anti-aff-udp-v2v"
TSG_ANTI_AFFINITY["CLIENT_IPS"]="%%ANTI_AFF_VM2_PORT1_IP%%"
TSG_ANTI_AFFINITY["CLIENT_MGMT_IPS"]="%%ANTI_AFF_VM2_MGMT_IP%%"
TSG_ANTI_AFFINITY["CLIENT_USERNAME"]="root"

# ext-server testsuite group
declare -gA TSG_EXT_SRV
TSG_EXT_SRV["NAME"]="ext-srv"
TSG_EXT_SRV["TESTSUITE_LIST"]="ext-srv-tcp-p2v|ext-srv-udp-p2v"
TSG_EXT_SRV["CLIENT_IPS"]="%%EXT_PHY_SERVER_IP%%"
TSG_EXT_SRV["CLIENT_MGMT_IPS"]="%%EXT_PHY_SERVER_MGMT_IP%%"
TSG_EXT_SRV["CLIENT_USERNAME"]="root"

# comp-node testsuite group
# There is no simple way to create network port from compute node to DPDK port.
#declare -gA TSG_COMP_NODE
#TSG_COMP_NODE["NAME"]="comp-node"
#TSG_COMP_NODE["TESTSUITE_LIST"]="comp-node-tcp-pc2pc|comp-node-tcp-pc2p|comp-node-udp-pc2pc|comp-node-udp-pc2p"
#TSG_COMP_NODE["CLIENT_IPS"]="%%COMP_PHY_SRV_Y_IP%%"
#TSG_COMP_NODE["CLIENT_MGMT_IPS"]="%%COMP_PHY_SRV_Y_MGMT_IP%%"
#TSG_COMP_NODE["CLIENT_USERNAME"]="root"
