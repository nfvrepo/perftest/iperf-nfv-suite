#!/usr/bin/env bash
########################################################################################
# File           : vm-install-pkgs.sh
# Version        : 0.3.0
# Author(s)      : Erol Ozcan <http://github.com/erolozcan (Argela)>
# Create date    : 2018-05-31
# Description    :
# iPerf NFV Test Suite VM script for cloud-init.
#########################################################################################

OS_DISTRO="${1}"

source ../../bin/bash-util-common-lib.sh

# *****************************
# usage
# *****************************
usage() {
    cat << ARGEOF
Usage: $0 <fedora|ubuntu>
      [other_params]
ARGEOF
}

# *****************************
# install_pkgs_fedora
# *****************************
install_pkgs_fedora() {
    echo "Install required packages for Fedora: starting..."
    CMD="yum install -y iperf3 jq nfs-utils screen pciutils bc ethtool"
    run_cmd "${CMD}"
    echo "Install required packages for Fedora: starting..."
}

# *****************************
# install_pkgs_ubuntu
# *****************************
install_pkgs_ubuntu() {
    echo "Install required packages for Ubuntu : starting..."
    CMD="apt-get install -y iperf3 jq nfs-utils screen pciutils bc ethtool"
    run_cmd "${CMD}"
    echo "Install required packages for Ubuntu: starting..."
}


case "${OS_DISTRO}" in
   ubuntu)
        install_pkgs_ubuntu
        ;;
   fedora)
        install_pkgs_fedora
        ;;
   *)
        usage
        ;;
esac