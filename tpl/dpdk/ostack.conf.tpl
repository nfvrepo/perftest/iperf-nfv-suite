#!/usr/bin/env bash
# iPerf NFV Test Suite DPDK OpenStack configuration

#@@OSTACK_RCFILE@@
export OSTACK_RCFILE="/home/stack/nfvovercloudrc.v3"

# External Physical server
export EXT_PHY_SERVER_HOSTNAME="ext-srv-hostname"
#@@EXT_PHY_SERVER_NIC@@
export EXT_PHY_SERVER_NIC="vmbr4071"
#@@EXT_PHY_SERVER_IP@@
export EXT_PHY_SERVER_IP="192.168.130.254"
#@@EXT_PHY_SERVER_MGMT_NIC@@
export EXT_PHY_SERVER_MGMT_NIC="vmbr0"
#@@EXT_PHY_SERVER_MGMT_IP@@
export EXT_PHY_SERVER_MGMT_IP="192.168.30.50"
#@@EXT_PHY_SERVER_TASKSET@@
export EXT_PHY_SERVER_TASKSET="6,30"
#@@EXT_PHY_SERVER_IPERF_AFF@@
export EXT_PHY_SERVER_IPERF_AFF="6"

# Physical Computer server X (host of anti-aff-vm2)
#@@COMP_PHY_SRV_X_HOSTNAME@@
export COMP_PHY_SRV_X_HOSTNAME="comphcidpdkhp380-0"
#@@COMP_PHY_SRV_X_DOMNAME@@
export COMP_PHY_SRV_X_DOMNAME="localdomain"
export COMP_PHY_SRV_X_HOST="${COMP_PHY_SRV_X_HOSTNAME}.${COMP_PHY_SRV_X_DOMNAME}"
#@@COMP_PHY_SRV_X_MGMT_NIC@@
export COMP_PHY_SRV_X_MGMT_NIC="eno1"
#@@COMP_PHY_SRV_X_MGMT_IP@@
export COMP_PHY_SRV_X_MGMT_IP="192.168.128.21"
#@@COMP_PHY_SRV_X_NIC@@
export COMP_PHY_SRV_X_NIC="ensf1f1.4071"
#@@COMP_PHY_SRV_X_IP@@
export COMP_PHY_SRV_X_IP="192.168.130.252"
#@@COMP_PHY_SRV_X_TASKSET@@
export COMP_PHY_SRV_X_TASKSET="6,8"
#@@COMP_PHY_SRV_X_IPERF_AFF@@
export COMP_PHY_SRV_X_IPERF_AFF="6"

# Physical Computer server Y (host of anti-aff-vm1)
#@@COMP_PHY_SRV_Y_HOSTNAME@@
export COMP_PHY_SRV_Y_HOSTNAME="comphcidpdkhp380-1"
#@@COMP_PHY_SRV_Y_DOMNAME@@
export COMP_PHY_SRV_Y_DOMNAME="localdomain"
export COMP_PHY_SRV_Y_HOST="${COMP_PHY_SRV_Y_HOSTNAME}.${COMP_PHY_SRV_Y_DOMNAME}"
#@@COMP_PHY_SRV_Y_MGMT_NIC@@
export COMP_PHY_SRV_Y_MGMT_NIC="eno1"
#@@COMP_PHY_SRV_Y_MGMT_IP@@
export COMP_PHY_SRV_Y_MGMT_IP="192.168.128.23"
#@@COMP_PHY_SRV_Y_NIC@@
export COMP_PHY_SRV_Y_NIC="ensf1f1.4071"
#@@COMP_PHY_SRV_Y_IP@@
export COMP_PHY_SRV_Y_IP="192.168.130.251"
#@@COMP_PHY_SRV_Y_TASKSET@@
export COMP_PHY_SRV_Y_TASKSET="6,8"
#@@COMP_PHY_SRV_Y_IPERF_AFF@@
export COMP_PHY_SRV_Y_IPERF_AFF="6"

#@@PERF_NET_NAME@@
export PERF_NET_NAME="dpdk4071"

# VM shared common properties
#@@VM_AVAILIBILITY_ZONE@@
export VM_AVAILIBILITY_ZONE="dpdk"
#@@VM_FLAVOR@@
export VM_FLAVOR="a2.medium_dpdk"
#export VM_FLAVOR="a1.medium_dpdk"
#@@VM_IMG_NAME@@
export VM_IMG_NAME="${VM_IMG_NAME_FEDORA}"
#@@VM_USERDATA@@
export VM_USERDATA="${INTS_ETC_DIR}/${INTS_PROFILE_SHORTNAME}/vm-userdata.yaml"

# VMs port names
export AFF_VM1_PORT1_NAME="${INTS_PROFILE_SHORTNAME}-aff_vm1-p1"
export AFF_VM2_PORT1_NAME="${INTS_PROFILE_SHORTNAME}-aff_vm2-p1"
export ANTI_AFF_VM1_PORT1_NAME="${INTS_PROFILE_SHORTNAME}-antiaff_vm1-p1"
export ANTI_AFF_VM2_PORT1_NAME="${INTS_PROFILE_SHORTNAME}-antiaff_vm2-p1"

profile_chk_create_port() {
    local l_net_name=$1
    local l_port_name=$2

    local l_tmp_exists_id=$(openstack port show ${l_port_name} -f value -c id  2> /dev/null)
    if [[ ! -z "${l_tmp_exists_id}" ]]; then
        echo "WARN: port ${l_port_name} exists with ID ${l_tmp_exists_id}"
        return 0
    fi

    openstack port create --network ${l_net_name} ${l_port_name}
}

profile_create_ports() {
    profile_chk_create_port ${PERF_NET_NAME} ${AFF_VM1_PORT1_NAME}
    profile_chk_create_port ${PERF_NET_NAME} ${AFF_VM2_PORT1_NAME}
    profile_chk_create_port ${PERF_NET_NAME} ${ANTI_AFF_VM1_PORT1_NAME}
    profile_chk_create_port ${PERF_NET_NAME} ${ANTI_AFF_VM2_PORT1_NAME}
}

profile_set_ports_env() {
    export AFF_VM1_PORT1_ID=$(openstack port show ${AFF_VM1_PORT1_NAME} -f value -c id  2> /dev/null)
    export AFF_VM2_PORT1_ID=$(openstack port show ${AFF_VM2_PORT1_NAME} -f value -c id  2> /dev/null)
    export ANTI_AFF_VM1_PORT1_ID=$(openstack port show ${ANTI_AFF_VM1_PORT1_NAME} -f value -c id  2> /dev/null)
    export ANTI_AFF_VM2_PORT1_ID=$(openstack port show ${ANTI_AFF_VM2_PORT1_NAME} -f value -c id  2> /dev/null)

    export AFF_VM1_PORT1_IP=$(openstack port show ${AFF_VM1_PORT1_NAME} -f value -c fixed_ips  2> /dev/null | grep "ip_address='[0-9]*\." | cut -d"'" -f2)
    export AFF_VM2_PORT1_IP=$(openstack port show ${AFF_VM2_PORT1_NAME} -f value -c fixed_ips  2> /dev/null | grep "ip_address='[0-9]*\." | cut -d"'" -f2)
    export ANTI_AFF_VM1_PORT1_IP=$(openstack port show ${ANTI_AFF_VM1_PORT1_NAME} -f value -c fixed_ips  2> /dev/null | grep "ip_address='[0-9]*\." | cut -d"'" -f2)
    export ANTI_AFF_VM2_PORT1_IP=$(openstack port show ${ANTI_AFF_VM2_PORT1_NAME} -f value -c fixed_ips  2> /dev/null | grep "ip_address='[0-9]*\." | cut -d"'" -f2)
}

run_once_at_init() {
    echo "DEBUG: START ostack.conf run_once_at_init"
    .  ${OSTACK_RCFILE}
    profile_create_ports
    profile_set_ports_env
    cat << ARGEOF
DEBUG: FINISH ostack.conf run_once_at_init. Ports:
${AFF_VM1_PORT1_NAME} - ${AFF_VM1_PORT1_ID} - ${AFF_VM1_PORT1_IP}
${AFF_VM2_PORT1_NAME} - ${AFF_VM2_PORT1_ID} - ${AFF_VM2_PORT1_IP}
${ANTI_AFF_VM1_PORT1_NAME} - ${ANTI_AFF_VM1_PORT1_ID} - ${ANTI_AFF_VM1_PORT1_IP}
${ANTI_AFF_VM2_PORT1_NAME} - ${ANTI_AFF_VM2_PORT1_ID} - ${ANTI_AFF_VM2_PORT1_IP}
ARGEOF
}

profile_chk_delete_port() {
    local l_port_id=$1

    local l_tmp_exists_id=$(openstack port show ${l_port_id} -f value -c id  2> /dev/null)
    if [[ -z "${l_tmp_exists_id}" ]]; then
        echo "WARN: port ${l_port_id} does not exists."
        return 0
    fi

    echo "DEBUG: Deleting port ${l_port_id} ..."
    openstack port delete ${l_port_id}
}


profile_delete_ports() {
    profile_set_ports_env

    profile_chk_delete_port ${AFF_VM1_PORT1_ID}
    profile_chk_delete_port ${AFF_VM2_PORT1_ID}
    profile_chk_delete_port ${ANTI_AFF_VM1_PORT1_ID}
    profile_chk_delete_port ${ANTI_AFF_VM2_PORT1_ID}
}
