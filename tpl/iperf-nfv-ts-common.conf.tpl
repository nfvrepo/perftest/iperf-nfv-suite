#@@MAIN_TITLE@@
MAIN_TITLE="Turk Telekom RHOSP 12 Network Throughput Performance Tests"
#@@RESULT_DIR@@
RESULT_DIR="${INTS_RESULT_DIR}"
#@@BANDWITH@@
BANDWITH="10g"
#@@TIME@@
TIME="180"
#@@OMIT_TIME@@
OMIT_TIME="3"
SERVER_MGMT_IP=""
#@@SERVER_PORT@@
SERVER_PORT="5201"
SERVER_TASKSET=""
SERVER_AFFINITY=""
#@@SERVER_USERNAME@@
SERVER_USERNAME="root"
CLIENT_HOSTNAME="$(hostname -s)"
# ip addr show eth0 | grep 'inet ' | awk '{ print $2}' | cut -d'/' -f1
CLIENT_IP="$(hostname -I)"
CLIENT_TASKSET=""
CLIENT_AFFINITY=""
#CLIENT_USERNAME="root"
#@@CHART_SERVER_IP_UNAME@@
CHART_SERVER_IP_UNAME="root@192.168.30.50"
#  ethheader + ipheader + tcpheader
#@@MSS_LIST@@
declare -ga MSS_LIST=("88" "280" "1524" "9024")
# paralel streams
#@@PARALLEL_LIST@@
declare -ga PARALLEL_LIST=("1" "10" "25" "50")
