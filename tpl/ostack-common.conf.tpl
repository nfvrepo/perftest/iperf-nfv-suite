#!/usr/bin/env bash
# Iperf NFV Test Suite OpenStack shared configuration.

#@@EXT_PHY_SERVER_MGMT_IP@@
export EXT_PHY_SERVER_MGMT_IP="192.168.30.50"

#@@IPERF_NFV_TS_DL_INST_URL@@
export IPERF_NFV_TS_DL_INST_URL="https://gitlab.com/nfvrepo/perftest/iperf-nfv-suite/raw/master/dl-install-iperf-nfv-ts.sh"

#@@VM_IMG_NAME_FEDORA@@
#export VM_IMG_NAME_FEDORA="Fedora-Cloud-Base-27-1.6.x86_64"
#export VM_IMG_URL_FEDORA="http://ftp.linux.org.tr/fedora/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2"
#export VM_IMG_URL_FEDORA="http://192.168.30.10/mirror/cloud/images/download.fedoraproject.org/pub/fedora/releases/27/CloudImages/x86_64/images/Fedora-Cloud-Base-27-1.6.x86_64.qcow2"
#export VM_IMG_NAME_FEDORA="Fedora-Cloud-Base-28-1.1.x86_64"
#export VM_IMG_URL_FEDORA="https://download.fedoraproject.org/pub/fedora/linux/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-28-1.1.x86_64.qcow2"
#export VM_IMG_URL_FEDORA="http://192.168.30.10/mirror/cloud/images/download.fedoraproject.org/pub/fedora/linux/releases/28/Cloud/x86_64/images/Fedora-Cloud-Base-28-1.1.x86_64.qcow2"
export VM_IMG_NAME_FEDORA="Fedora-Cloud-Base-27-1.6.x86_64-ints-v1"
export VM_IMG_URL_FEDORA="http://192.168.30.10/mirror/cloud/images/Fedora-Cloud-Base-27-1.6.x86_64-ints-v1.raw2"
#@@VM_IMG_NAME_UBUNTU@@
export VM_IMG_NAME_UBUNTU="ubuntu-18.04-server-cloudimg-amd64"
export VM_IMG_URL_UBUNTU="https://cloud-images.ubuntu.com/releases/18.04/release-20180617/ubuntu-18.04-server-cloudimg-amd64.img"
#export VM_IMG_URL_UBUNTU="http://192.168.30.10/mirror/cloud/images/cloud-images.ubuntu.com/releases/18.04/release-20180617/ubuntu-18.04-server-cloudimg-amd64.img"
#@@SGP_NAME_AFFINITY@@
export SGP_NAME_AFFINITY="affinity-grp"
#@@SGP_NAME_ANTI_AFF@@
export SGP_NAME_ANTI_AFF="anti-affinity-grp"
