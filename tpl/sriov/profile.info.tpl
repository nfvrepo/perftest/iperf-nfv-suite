# Profile Information
export INTS_PROFILE_SHORTNAME="sriov"
export INTS_PROFILE_TITLENAME="SR-IOV"
export INTS_PROFILE_INIT_STATE="not_set"
export INTS_PROFILE_INIT_START=""
export INTS_PROFILE_INIT_FINISH=""
declare -a INTS_PROFILE_INIT_FILES=("ostack.conf.tpl" "ostack-vms.conf.tpl" "testsuite-group.conf.tpl" "iperf-nfv-ts.conf.tpl" "vm-install-pkgs.sh.tpl" "vm-userdata.yaml.tpl")
declare -a INTS_PROFILE_INIT_SRC_ORDER=("profile.info" "ostack.conf" "ostack-vms.conf" "testsuite-group.conf" "iperf-nfv-ts.conf")
declare -a INTS_PROFILE_INIT_TPL_VAR_REPLACE=("vm-install-pkgs.sh" "vm-userdata.yaml")
declare -a INTS_PROFILE_SOURCE_ORDER=("ostack.conf" "ostack-vms.conf" "testsuite-group.conf" "iperf-nfv-ts.conf")
