#!/usr/bin/env bash
# iPerf NFV Test Suite SR-IOV configuration of VMs

# aff-vm1
export AFF_VM1_HOSTNAME="perf-%%INTS_PROFILE_SHORTNAME%%-aff-vm1"
export AFF_VM1_PORT1_NAME="%%AFF_VM1_PORT1_NAME%%"
export AFF_VM1_PORT1_ID="%%AFF_VM1_PORT1_ID%%"
export AFF_VM1_PORT1_IP="%%AFF_VM1_PORT1_IP%%"
export AFF_VM1_PORT1_NIC="eth0"
export AFF_VM1_AZ="%%VM_AVAILIBILITY_ZONE%%"
export AFF_VM1_HOST="%%COMP_PHY_SRV_X_HOST%%"
export AFF_VM1_MGMT_IP="${AFF_VM1_PORT1_IP}"
export AFF_VM1_MGMT_NIC="${AFF_VM1_PORT1_NIC}"
export AFF_VM1_TASKSET=""
export AFF_VM1_IPERF_AFF=""

# aff-vm2
export AFF_VM2_HOSTNAME="perf-%%INTS_PROFILE_SHORTNAME%%-aff-vm2"
export AFF_VM2_PORT1_NAME="%%AFF_VM2_PORT1_NAME%%"
export AFF_VM2_PORT1_ID="%%AFF_VM2_PORT1_ID%%"
export AFF_VM2_PORT1_IP="%%AFF_VM2_PORT1_IP%%"
export AFF_VM2_PORT1_NIC="eth0"
export AFF_VM2_AZ="%%VM_AVAILIBILITY_ZONE%%"
export AFF_VM2_HOST="%%COMP_PHY_SRV_X_HOST%%"
export AFF_VM2_MGMT_IP="${AFF_VM2_PORT1_IP}"
export AFF_VM2_MGMT_NIC="${AFF_VM2_PORT1_NIC}"
export AFF_VM2_TASKSET=""
export AFF_VM2_IPERF_AFF=""

# anti-aff-vm1
export ANTI_AFF_VM1_HOSTNAME="perf-%%INTS_PROFILE_SHORTNAME%%-anti-aff-vm1"
export ANTI_AFF_VM1_PORT1_NAME="%%ANTI_AFF_VM1_PORT1_NAME%%"
export ANTI_AFF_VM1_PORT1_ID="%%ANTI_AFF_VM1_PORT1_ID%%"
export ANTI_AFF_VM1_PORT1_IP="%%ANTI_AFF_VM1_PORT1_IP%%"
export ANTI_AFF_VM1_PORT1_NIC="eth0"
export ANTI_AFF_VM1_AZ="%%VM_AVAILIBILITY_ZONE%%"
export ANTI_AFF_VM1_HOST="%%COMP_PHY_SRV_X_HOST%%"
export ANTI_AFF_VM1_MGMT_IP="${ANTI_AFF_VM1_PORT1_IP}"
export ANTI_AFF_VM1_MGMT_NIC="${ANTI_AFF_VM1_PORT1_NIC}"
export ANTI_AFF_VM1_TASKSET=""
export ANTI_AFF_VM1_IPERF_AFF=""

# anti-aff-vm2
export ANTI_AFF_VM2_HOSTNAME="perf-%%INTS_PROFILE_SHORTNAME%%-anti-aff-vm2"
export ANTI_AFF_VM2_PORT1_NAME="%%ANTI_AFF_VM2_PORT1_NAME%%"
export ANTI_AFF_VM2_PORT1_ID="%%ANTI_AFF_VM2_PORT1_ID%%"
export ANTI_AFF_VM2_PORT1_IP="%%ANTI_AFF_VM2_PORT1_IP%%"
export ANTI_AFF_VM2_PORT1_NIC="eth0"
export ANTI_AFF_VM2_AZ="%%VM_AVAILIBILITY_ZONE%%"
export ANTI_AFF_VM2_HOST="%%COMP_PHY_SRV_Y_HOST%%"
export ANTI_AFF_VM2_MGMT_IP="${ANTI_AFF_VM2_PORT1_IP}"
export ANTI_AFF_VM2_MGMT_NIC="${ANTI_AFF_VM2_PORT1_NIC}"
export ANTI_AFF_VM2_TASKSET=""
export ANTI_AFF_VM2_IPERF_AFF=""


profile_create_vm() {
    local l_vm_env_prefix=$1

    local l_port_id_nm="${l_vm_env_prefix}_PORT1_ID"
    local l_az_nm="${l_vm_env_prefix}_AZ"
    local l_host_nm="${l_vm_env_prefix}_HOST"
    local l_vm_name_nm="${l_vm_env_prefix}_HOSTNAME"

    local l_vm_create_cmd="openstack server create \
--key-name default --flavor ${VM_FLAVOR} \
--image ${VM_IMG_NAME} \
--nic port-id=${!l_port_id_nm} \
--user-data ${VM_USERDATA} \
--availability-zone ${!l_az_nm}:${!l_host_nm} \
--wait \
${!l_vm_name_nm}"
    echo "${l_vm_create_cmd}"
    eval "${l_vm_create_cmd}"
}

profile_create_vms() {
    local l_vm_no=""
    for l_vm_no in "AFF_VM1" "AFF_VM2" "ANTI_AFF_VM1" "ANTI_AFF_VM2"; do
        profile_create_vm ${l_vm_no}
    done
}


profile_delete_vm() {
    local l_vm_name=$1

    local l_del_cmd="openstack server delete --wait ${l_vm_name}"
    echo "${l_del_cmd}"
    eval "${l_del_cmd}"
}

profile_delete_vms() {
    local l_vm_no=""
    local l_vm_name_nm=""
    for l_vm_no in "AFF_VM1" "AFF_VM2" "ANTI_AFF_VM1" "ANTI_AFF_VM2"; do
        l_vm_name="${l_vm_no}_HOSTNAME"
        profile_delete_vm "${!l_vm_name}"
    done
}